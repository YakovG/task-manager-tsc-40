package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Project;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByNameStartCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-start-by-name";

    @NotNull public static final String DESCRIPTION ="Start project by name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = endpointLocator.getProjectEndpoint().startProjectByName(session, name);
        if (project == null) throw new ProjectNotFoundException();
    }
}
