package ru.goloshchapov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Project;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListSortCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-sorted-list";

    @NotNull public static final String DESCRIPTION = "Show sorted project list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[PROJECT SORTED LIST]");
        System.out.println("ENTER SORT TYPE");
        System.out.println(Arrays.toString(Sort.values()));
        System.out.println("DEFAULT TYPE = CREATED");
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final List<Project> projects = endpointLocator.getProjectEndpoint().sortedProjectBy(session, sortType);
        int index = 1;
        for (@NotNull final Project project: projects) {
            System.out.println(index + ". " + project.getId() + " : " + project.getName());
            index++;
        }
    }
}
