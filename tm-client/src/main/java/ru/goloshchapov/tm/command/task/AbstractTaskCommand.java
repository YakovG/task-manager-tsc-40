package ru.goloshchapov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final Task task) {
        @Nullable Session session = endpointLocator.getSession();
        endpointLocator.getTaskEndpoint().checkTaskAccess(session);
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println(("STATUS: " + task.getStatus()));
        System.out.println("CREATED: " + task.getCreated());
        if (task.getDateStart() != null) System.out.println("STARTED: " + task.getDateStart());
        if (task.getDateFinish() != null) System.out.println("FINISHED: " + task.getDateFinish());
    }

}