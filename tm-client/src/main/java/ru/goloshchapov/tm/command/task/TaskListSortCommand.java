package ru.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListSortCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-sorted-list";

    @NotNull public static final String DESCRIPTION = "Show task sorted list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[TASK SORTED LIST]");
        System.out.println("ENTER SORT TYPE");
        System.out.println(Arrays.toString(Sort.values()));
        System.out.println("DEFAULT TYPE = CREATED");
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = endpointLocator.getTaskEndpoint().sortedTaskBy(session, sortType);
        int index = 1;
        for (@NotNull final Task task: tasks) {
            System.out.println(index + ". " + task.getId() + " : " + task.getName());
            index++;
        }
    }
}
