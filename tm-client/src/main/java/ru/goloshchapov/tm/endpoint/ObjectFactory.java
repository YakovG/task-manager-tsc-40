
package ru.goloshchapov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.goloshchapov.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddProjectAllWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addProjectAllWithoutUserId");
    private final static QName _AddProjectAllWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addProjectAllWithoutUserIdResponse");
    private final static QName _AddProjectWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addProjectWithoutUserId");
    private final static QName _AddProjectWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addProjectWithoutUserIdResponse");
    private final static QName _AddTaskAllWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addTaskAllWithoutUserId");
    private final static QName _AddTaskAllWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addTaskAllWithoutUserIdResponse");
    private final static QName _AddTaskWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addTaskWithoutUserId");
    private final static QName _AddTaskWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addTaskWithoutUserIdResponse");
    private final static QName _AddUser_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addUser");
    private final static QName _AddUserAll_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addUserAll");
    private final static QName _AddUserAllResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addUserAllResponse");
    private final static QName _AddUserResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "addUserResponse");
    private final static QName _ClearBackup_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearBackup");
    private final static QName _ClearBackupResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearBackupResponse");
    private final static QName _ClearProjectWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearProjectWithoutUserId");
    private final static QName _ClearProjectWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearProjectWithoutUserIdResponse");
    private final static QName _ClearTaskWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearTaskWithoutUserId");
    private final static QName _ClearTaskWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearTaskWithoutUserIdResponse");
    private final static QName _ClearUser_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearUser");
    private final static QName _ClearUserResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "clearUserResponse");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUserResponse");
    private final static QName _CreateUserWithAll_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUserWithAll");
    private final static QName _CreateUserWithAllResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUserWithAllResponse");
    private final static QName _CreateUserWithEmail_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUserWithEmail");
    private final static QName _CreateUserWithEmailResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUserWithEmailResponse");
    private final static QName _CreateUserWithRole_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUserWithRole");
    private final static QName _CreateUserWithRoleResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "createUserWithRoleResponse");
    private final static QName _FindProjectAllWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectAllWithoutUserId");
    private final static QName _FindProjectAllWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectAllWithoutUserIdResponse");
    private final static QName _FindProjectByIdWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectByIdWithoutUserId");
    private final static QName _FindProjectByIdWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectByIdWithoutUserIdResponse");
    private final static QName _FindProjectByIndexWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectByIndexWithoutUserId");
    private final static QName _FindProjectByIndexWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectByIndexWithoutUserIdResponse");
    private final static QName _FindProjectByNameWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectByNameWithoutUserId");
    private final static QName _FindProjectByNameWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findProjectByNameWithoutUserIdResponse");
    private final static QName _FindTaskAllWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskAllWithoutUserId");
    private final static QName _FindTaskAllWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskAllWithoutUserIdResponse");
    private final static QName _FindTaskByIdWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskByIdWithoutUserId");
    private final static QName _FindTaskByIdWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskByIdWithoutUserIdResponse");
    private final static QName _FindTaskByIndexWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskByIndexWithoutUserId");
    private final static QName _FindTaskByIndexWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskByIndexWithoutUserIdResponse");
    private final static QName _FindTaskByNameWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskByNameWithoutUserId");
    private final static QName _FindTaskByNameWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findTaskByNameWithoutUserIdResponse");
    private final static QName _FindUserAll_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserAll");
    private final static QName _FindUserAllResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserAllResponse");
    private final static QName _FindUserByEmail_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserByEmail");
    private final static QName _FindUserByEmailResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserByEmailResponse");
    private final static QName _FindUserById_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserByIdResponse");
    private final static QName _FindUserByIndex_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserByIndex");
    private final static QName _FindUserByIndexResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserByIndexResponse");
    private final static QName _FindUserByLogin_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserByLogin");
    private final static QName _FindUserByLoginResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "findUserByLoginResponse");
    private final static QName _GetProjectIdByIndexWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getProjectIdByIndexWithoutUserId");
    private final static QName _GetProjectIdByIndexWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getProjectIdByIndexWithoutUserIdResponse");
    private final static QName _GetProjectIdByNameWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getProjectIdByNameWithoutUserId");
    private final static QName _GetProjectIdByNameWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getProjectIdByNameWithoutUserIdResponse");
    private final static QName _GetTaskIdByIndexWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getTaskIdByIndexWithoutUserId");
    private final static QName _GetTaskIdByIndexWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getTaskIdByIndexWithoutUserIdResponse");
    private final static QName _GetTaskIdByNameWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getTaskIdByNameWithoutUserId");
    private final static QName _GetTaskIdByNameWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getTaskIdByNameWithoutUserIdResponse");
    private final static QName _GetUserIdByIndex_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getUserIdByIndex");
    private final static QName _GetUserIdByIndexResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "getUserIdByIndexResponse");
    private final static QName _IsEmailExists_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isEmailExists");
    private final static QName _IsEmailExistsResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isEmailExistsResponse");
    private final static QName _IsProjectAbsentByIdWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isProjectAbsentByIdWithoutUserId");
    private final static QName _IsProjectAbsentByIdWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isProjectAbsentByIdWithoutUserIdResponse");
    private final static QName _IsProjectAbsentByIndexWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isProjectAbsentByIndexWithoutUserId");
    private final static QName _IsProjectAbsentByIndexWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isProjectAbsentByIndexWithoutUserIdResponse");
    private final static QName _IsProjectAbsentByNameWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isProjectAbsentByNameWithoutUserId");
    private final static QName _IsProjectAbsentByNameWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isProjectAbsentByNameWithoutUserIdResponse");
    private final static QName _IsTaskAbsentByIdWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isTaskAbsentByIdWithoutUserId");
    private final static QName _IsTaskAbsentByIdWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isTaskAbsentByIdWithoutUserIdResponse");
    private final static QName _IsTaskAbsentByIndexWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isTaskAbsentByIndexWithoutUserId");
    private final static QName _IsTaskAbsentByIndexWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isTaskAbsentByIndexWithoutUserIdResponse");
    private final static QName _IsTaskAbsentByNameWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isTaskAbsentByNameWithoutUserId");
    private final static QName _IsTaskAbsentByNameWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isTaskAbsentByNameWithoutUserIdResponse");
    private final static QName _IsUserAbsentById_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isUserAbsentById");
    private final static QName _IsUserAbsentByIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isUserAbsentByIdResponse");
    private final static QName _IsUserAbsentByIndex_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isUserAbsentByIndex");
    private final static QName _IsUserAbsentByIndexResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isUserAbsentByIndexResponse");
    private final static QName _IsUserLoginExists_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isUserLoginExists");
    private final static QName _IsUserLoginExistsResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "isUserLoginExistsResponse");
    private final static QName _LoadBackup_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadBackup");
    private final static QName _LoadBackupResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadBackupResponse");
    private final static QName _LoadBase64Data_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadBase64Data");
    private final static QName _LoadBase64DataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadBase64DataResponse");
    private final static QName _LoadBinaryData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadBinaryData");
    private final static QName _LoadBinaryDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadBinaryDataResponse");
    private final static QName _LoadJsonFasterXmlData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadJsonFasterXmlData");
    private final static QName _LoadJsonFasterXmlDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadJsonFasterXmlDataResponse");
    private final static QName _LoadJsonJaxBData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadJsonJaxBData");
    private final static QName _LoadJsonJaxBDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadJsonJaxBDataResponse");
    private final static QName _LoadXmlFasterXmlData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadXmlFasterXmlData");
    private final static QName _LoadXmlFasterXmlDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadXmlFasterXmlDataResponse");
    private final static QName _LoadXmlJaxBData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadXmlJaxBData");
    private final static QName _LoadXmlJaxBDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadXmlJaxBDataResponse");
    private final static QName _LoadYamlData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadYamlData");
    private final static QName _LoadYamlDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "loadYamlDataResponse");
    private final static QName _LockUserByEmail_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "lockUserByEmail");
    private final static QName _LockUserByEmailResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "lockUserByEmailResponse");
    private final static QName _LockUserByLogin_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "lockUserByLogin");
    private final static QName _LockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "lockUserByLoginResponse");
    private final static QName _RemoveProjectByIdWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeProjectByIdWithoutUserId");
    private final static QName _RemoveProjectByIdWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeProjectByIdWithoutUserIdResponse");
    private final static QName _RemoveProjectByIndexWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeProjectByIndexWithoutUserId");
    private final static QName _RemoveProjectByIndexWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeProjectByIndexWithoutUserIdResponse");
    private final static QName _RemoveProjectWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeProjectWithoutUserId");
    private final static QName _RemoveProjectWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeProjectWithoutUserIdResponse");
    private final static QName _RemoveTaskByIdWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeTaskByIdWithoutUserId");
    private final static QName _RemoveTaskByIdWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeTaskByIdWithoutUserIdResponse");
    private final static QName _RemoveTaskWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeTaskWithoutUserId");
    private final static QName _RemoveTaskWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeTaskWithoutUserIdResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUser");
    private final static QName _RemoveUserByEmail_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserByEmail");
    private final static QName _RemoveUserByEmailResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserByEmailResponse");
    private final static QName _RemoveUserById_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserById");
    private final static QName _RemoveUserByIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserByIdResponse");
    private final static QName _RemoveUserByIndex_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserByIndex");
    private final static QName _RemoveUserByIndexResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserByIndexResponse");
    private final static QName _RemoveUserByLogin_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserByLogin");
    private final static QName _RemoveUserByLoginResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserByLoginResponse");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "removeUserResponse");
    private final static QName _SaveBackup_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveBackup");
    private final static QName _SaveBackupResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveBackupResponse");
    private final static QName _SaveBase64Data_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveBase64Data");
    private final static QName _SaveBase64DataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveBase64DataResponse");
    private final static QName _SaveBinaryData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveBinaryData");
    private final static QName _SaveBinaryDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveBinaryDataResponse");
    private final static QName _SaveJsonFasterXmlData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveJsonFasterXmlData");
    private final static QName _SaveJsonFasterXmlDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveJsonFasterXmlDataResponse");
    private final static QName _SaveJsonJaxBData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveJsonJaxBData");
    private final static QName _SaveJsonJaxBDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveJsonJaxBDataResponse");
    private final static QName _SaveXmlFasterXmlData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveXmlFasterXmlData");
    private final static QName _SaveXmlFasterXmlDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveXmlFasterXmlDataResponse");
    private final static QName _SaveXmlJaxBData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveXmlJaxBData");
    private final static QName _SaveXmlJaxBDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveXmlJaxBDataResponse");
    private final static QName _SaveYamlData_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveYamlData");
    private final static QName _SaveYamlDataResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "saveYamlDataResponse");
    private final static QName _ShowAllUserWithProject_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "showAllUserWithProject");
    private final static QName _ShowAllUserWithProjectResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "showAllUserWithProjectResponse");
    private final static QName _ShowUserList_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "showUserList");
    private final static QName _ShowUserListResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "showUserListResponse");
    private final static QName _SizeProjectWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "sizeProjectWithoutUserId");
    private final static QName _SizeProjectWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "sizeProjectWithoutUserIdResponse");
    private final static QName _SizeTaskWithoutUserId_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "sizeTaskWithoutUserId");
    private final static QName _SizeTaskWithoutUserIdResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "sizeTaskWithoutUserIdResponse");
    private final static QName _SizeUser_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "sizeUser");
    private final static QName _SizeUserResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "sizeUserResponse");
    private final static QName _UnlockUserByEmail_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "unlockUserByEmail");
    private final static QName _UnlockUserByEmailResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "unlockUserByEmailResponse");
    private final static QName _UnlockUserByLogin_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "unlockUserByLogin");
    private final static QName _UnlockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.goloshchapov.ru/", "unlockUserByLoginResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.goloshchapov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddProjectAllWithoutUserId }
     * 
     */
    public AddProjectAllWithoutUserId createAddProjectAllWithoutUserId() {
        return new AddProjectAllWithoutUserId();
    }

    /**
     * Create an instance of {@link AddProjectAllWithoutUserIdResponse }
     * 
     */
    public AddProjectAllWithoutUserIdResponse createAddProjectAllWithoutUserIdResponse() {
        return new AddProjectAllWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link AddProjectWithoutUserId }
     * 
     */
    public AddProjectWithoutUserId createAddProjectWithoutUserId() {
        return new AddProjectWithoutUserId();
    }

    /**
     * Create an instance of {@link AddProjectWithoutUserIdResponse }
     * 
     */
    public AddProjectWithoutUserIdResponse createAddProjectWithoutUserIdResponse() {
        return new AddProjectWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link AddTaskAllWithoutUserId }
     * 
     */
    public AddTaskAllWithoutUserId createAddTaskAllWithoutUserId() {
        return new AddTaskAllWithoutUserId();
    }

    /**
     * Create an instance of {@link AddTaskAllWithoutUserIdResponse }
     * 
     */
    public AddTaskAllWithoutUserIdResponse createAddTaskAllWithoutUserIdResponse() {
        return new AddTaskAllWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link AddTaskWithoutUserId }
     * 
     */
    public AddTaskWithoutUserId createAddTaskWithoutUserId() {
        return new AddTaskWithoutUserId();
    }

    /**
     * Create an instance of {@link AddTaskWithoutUserIdResponse }
     * 
     */
    public AddTaskWithoutUserIdResponse createAddTaskWithoutUserIdResponse() {
        return new AddTaskWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link AddUser }
     * 
     */
    public AddUser createAddUser() {
        return new AddUser();
    }

    /**
     * Create an instance of {@link AddUserAll }
     * 
     */
    public AddUserAll createAddUserAll() {
        return new AddUserAll();
    }

    /**
     * Create an instance of {@link AddUserAllResponse }
     * 
     */
    public AddUserAllResponse createAddUserAllResponse() {
        return new AddUserAllResponse();
    }

    /**
     * Create an instance of {@link AddUserResponse }
     * 
     */
    public AddUserResponse createAddUserResponse() {
        return new AddUserResponse();
    }

    /**
     * Create an instance of {@link ClearBackup }
     * 
     */
    public ClearBackup createClearBackup() {
        return new ClearBackup();
    }

    /**
     * Create an instance of {@link ClearBackupResponse }
     * 
     */
    public ClearBackupResponse createClearBackupResponse() {
        return new ClearBackupResponse();
    }

    /**
     * Create an instance of {@link ClearProjectWithoutUserId }
     * 
     */
    public ClearProjectWithoutUserId createClearProjectWithoutUserId() {
        return new ClearProjectWithoutUserId();
    }

    /**
     * Create an instance of {@link ClearProjectWithoutUserIdResponse }
     * 
     */
    public ClearProjectWithoutUserIdResponse createClearProjectWithoutUserIdResponse() {
        return new ClearProjectWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link ClearTaskWithoutUserId }
     * 
     */
    public ClearTaskWithoutUserId createClearTaskWithoutUserId() {
        return new ClearTaskWithoutUserId();
    }

    /**
     * Create an instance of {@link ClearTaskWithoutUserIdResponse }
     * 
     */
    public ClearTaskWithoutUserIdResponse createClearTaskWithoutUserIdResponse() {
        return new ClearTaskWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link ClearUser }
     * 
     */
    public ClearUser createClearUser() {
        return new ClearUser();
    }

    /**
     * Create an instance of {@link ClearUserResponse }
     * 
     */
    public ClearUserResponse createClearUserResponse() {
        return new ClearUserResponse();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link CreateUserWithAll }
     * 
     */
    public CreateUserWithAll createCreateUserWithAll() {
        return new CreateUserWithAll();
    }

    /**
     * Create an instance of {@link CreateUserWithAllResponse }
     * 
     */
    public CreateUserWithAllResponse createCreateUserWithAllResponse() {
        return new CreateUserWithAllResponse();
    }

    /**
     * Create an instance of {@link CreateUserWithEmail }
     * 
     */
    public CreateUserWithEmail createCreateUserWithEmail() {
        return new CreateUserWithEmail();
    }

    /**
     * Create an instance of {@link CreateUserWithEmailResponse }
     * 
     */
    public CreateUserWithEmailResponse createCreateUserWithEmailResponse() {
        return new CreateUserWithEmailResponse();
    }

    /**
     * Create an instance of {@link CreateUserWithRole }
     * 
     */
    public CreateUserWithRole createCreateUserWithRole() {
        return new CreateUserWithRole();
    }

    /**
     * Create an instance of {@link CreateUserWithRoleResponse }
     * 
     */
    public CreateUserWithRoleResponse createCreateUserWithRoleResponse() {
        return new CreateUserWithRoleResponse();
    }

    /**
     * Create an instance of {@link FindProjectAllWithoutUserId }
     * 
     */
    public FindProjectAllWithoutUserId createFindProjectAllWithoutUserId() {
        return new FindProjectAllWithoutUserId();
    }

    /**
     * Create an instance of {@link FindProjectAllWithoutUserIdResponse }
     * 
     */
    public FindProjectAllWithoutUserIdResponse createFindProjectAllWithoutUserIdResponse() {
        return new FindProjectAllWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindProjectByIdWithoutUserId }
     * 
     */
    public FindProjectByIdWithoutUserId createFindProjectByIdWithoutUserId() {
        return new FindProjectByIdWithoutUserId();
    }

    /**
     * Create an instance of {@link FindProjectByIdWithoutUserIdResponse }
     * 
     */
    public FindProjectByIdWithoutUserIdResponse createFindProjectByIdWithoutUserIdResponse() {
        return new FindProjectByIdWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindProjectByIndexWithoutUserId }
     * 
     */
    public FindProjectByIndexWithoutUserId createFindProjectByIndexWithoutUserId() {
        return new FindProjectByIndexWithoutUserId();
    }

    /**
     * Create an instance of {@link FindProjectByIndexWithoutUserIdResponse }
     * 
     */
    public FindProjectByIndexWithoutUserIdResponse createFindProjectByIndexWithoutUserIdResponse() {
        return new FindProjectByIndexWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindProjectByNameWithoutUserId }
     * 
     */
    public FindProjectByNameWithoutUserId createFindProjectByNameWithoutUserId() {
        return new FindProjectByNameWithoutUserId();
    }

    /**
     * Create an instance of {@link FindProjectByNameWithoutUserIdResponse }
     * 
     */
    public FindProjectByNameWithoutUserIdResponse createFindProjectByNameWithoutUserIdResponse() {
        return new FindProjectByNameWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindTaskAllWithoutUserId }
     * 
     */
    public FindTaskAllWithoutUserId createFindTaskAllWithoutUserId() {
        return new FindTaskAllWithoutUserId();
    }

    /**
     * Create an instance of {@link FindTaskAllWithoutUserIdResponse }
     * 
     */
    public FindTaskAllWithoutUserIdResponse createFindTaskAllWithoutUserIdResponse() {
        return new FindTaskAllWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindTaskByIdWithoutUserId }
     * 
     */
    public FindTaskByIdWithoutUserId createFindTaskByIdWithoutUserId() {
        return new FindTaskByIdWithoutUserId();
    }

    /**
     * Create an instance of {@link FindTaskByIdWithoutUserIdResponse }
     * 
     */
    public FindTaskByIdWithoutUserIdResponse createFindTaskByIdWithoutUserIdResponse() {
        return new FindTaskByIdWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindTaskByIndexWithoutUserId }
     * 
     */
    public FindTaskByIndexWithoutUserId createFindTaskByIndexWithoutUserId() {
        return new FindTaskByIndexWithoutUserId();
    }

    /**
     * Create an instance of {@link FindTaskByIndexWithoutUserIdResponse }
     * 
     */
    public FindTaskByIndexWithoutUserIdResponse createFindTaskByIndexWithoutUserIdResponse() {
        return new FindTaskByIndexWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindTaskByNameWithoutUserId }
     * 
     */
    public FindTaskByNameWithoutUserId createFindTaskByNameWithoutUserId() {
        return new FindTaskByNameWithoutUserId();
    }

    /**
     * Create an instance of {@link FindTaskByNameWithoutUserIdResponse }
     * 
     */
    public FindTaskByNameWithoutUserIdResponse createFindTaskByNameWithoutUserIdResponse() {
        return new FindTaskByNameWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link FindUserAll }
     * 
     */
    public FindUserAll createFindUserAll() {
        return new FindUserAll();
    }

    /**
     * Create an instance of {@link FindUserAllResponse }
     * 
     */
    public FindUserAllResponse createFindUserAllResponse() {
        return new FindUserAllResponse();
    }

    /**
     * Create an instance of {@link FindUserByEmail }
     * 
     */
    public FindUserByEmail createFindUserByEmail() {
        return new FindUserByEmail();
    }

    /**
     * Create an instance of {@link FindUserByEmailResponse }
     * 
     */
    public FindUserByEmailResponse createFindUserByEmailResponse() {
        return new FindUserByEmailResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByIndex }
     * 
     */
    public FindUserByIndex createFindUserByIndex() {
        return new FindUserByIndex();
    }

    /**
     * Create an instance of {@link FindUserByIndexResponse }
     * 
     */
    public FindUserByIndexResponse createFindUserByIndexResponse() {
        return new FindUserByIndexResponse();
    }

    /**
     * Create an instance of {@link FindUserByLogin }
     * 
     */
    public FindUserByLogin createFindUserByLogin() {
        return new FindUserByLogin();
    }

    /**
     * Create an instance of {@link FindUserByLoginResponse }
     * 
     */
    public FindUserByLoginResponse createFindUserByLoginResponse() {
        return new FindUserByLoginResponse();
    }

    /**
     * Create an instance of {@link GetProjectIdByIndexWithoutUserId }
     * 
     */
    public GetProjectIdByIndexWithoutUserId createGetProjectIdByIndexWithoutUserId() {
        return new GetProjectIdByIndexWithoutUserId();
    }

    /**
     * Create an instance of {@link GetProjectIdByIndexWithoutUserIdResponse }
     * 
     */
    public GetProjectIdByIndexWithoutUserIdResponse createGetProjectIdByIndexWithoutUserIdResponse() {
        return new GetProjectIdByIndexWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link GetProjectIdByNameWithoutUserId }
     * 
     */
    public GetProjectIdByNameWithoutUserId createGetProjectIdByNameWithoutUserId() {
        return new GetProjectIdByNameWithoutUserId();
    }

    /**
     * Create an instance of {@link GetProjectIdByNameWithoutUserIdResponse }
     * 
     */
    public GetProjectIdByNameWithoutUserIdResponse createGetProjectIdByNameWithoutUserIdResponse() {
        return new GetProjectIdByNameWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link GetTaskIdByIndexWithoutUserId }
     * 
     */
    public GetTaskIdByIndexWithoutUserId createGetTaskIdByIndexWithoutUserId() {
        return new GetTaskIdByIndexWithoutUserId();
    }

    /**
     * Create an instance of {@link GetTaskIdByIndexWithoutUserIdResponse }
     * 
     */
    public GetTaskIdByIndexWithoutUserIdResponse createGetTaskIdByIndexWithoutUserIdResponse() {
        return new GetTaskIdByIndexWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link GetTaskIdByNameWithoutUserId }
     * 
     */
    public GetTaskIdByNameWithoutUserId createGetTaskIdByNameWithoutUserId() {
        return new GetTaskIdByNameWithoutUserId();
    }

    /**
     * Create an instance of {@link GetTaskIdByNameWithoutUserIdResponse }
     * 
     */
    public GetTaskIdByNameWithoutUserIdResponse createGetTaskIdByNameWithoutUserIdResponse() {
        return new GetTaskIdByNameWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link GetUserIdByIndex }
     * 
     */
    public GetUserIdByIndex createGetUserIdByIndex() {
        return new GetUserIdByIndex();
    }

    /**
     * Create an instance of {@link GetUserIdByIndexResponse }
     * 
     */
    public GetUserIdByIndexResponse createGetUserIdByIndexResponse() {
        return new GetUserIdByIndexResponse();
    }

    /**
     * Create an instance of {@link IsEmailExists }
     * 
     */
    public IsEmailExists createIsEmailExists() {
        return new IsEmailExists();
    }

    /**
     * Create an instance of {@link IsEmailExistsResponse }
     * 
     */
    public IsEmailExistsResponse createIsEmailExistsResponse() {
        return new IsEmailExistsResponse();
    }

    /**
     * Create an instance of {@link IsProjectAbsentByIdWithoutUserId }
     * 
     */
    public IsProjectAbsentByIdWithoutUserId createIsProjectAbsentByIdWithoutUserId() {
        return new IsProjectAbsentByIdWithoutUserId();
    }

    /**
     * Create an instance of {@link IsProjectAbsentByIdWithoutUserIdResponse }
     * 
     */
    public IsProjectAbsentByIdWithoutUserIdResponse createIsProjectAbsentByIdWithoutUserIdResponse() {
        return new IsProjectAbsentByIdWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link IsProjectAbsentByIndexWithoutUserId }
     * 
     */
    public IsProjectAbsentByIndexWithoutUserId createIsProjectAbsentByIndexWithoutUserId() {
        return new IsProjectAbsentByIndexWithoutUserId();
    }

    /**
     * Create an instance of {@link IsProjectAbsentByIndexWithoutUserIdResponse }
     * 
     */
    public IsProjectAbsentByIndexWithoutUserIdResponse createIsProjectAbsentByIndexWithoutUserIdResponse() {
        return new IsProjectAbsentByIndexWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link IsProjectAbsentByNameWithoutUserId }
     * 
     */
    public IsProjectAbsentByNameWithoutUserId createIsProjectAbsentByNameWithoutUserId() {
        return new IsProjectAbsentByNameWithoutUserId();
    }

    /**
     * Create an instance of {@link IsProjectAbsentByNameWithoutUserIdResponse }
     * 
     */
    public IsProjectAbsentByNameWithoutUserIdResponse createIsProjectAbsentByNameWithoutUserIdResponse() {
        return new IsProjectAbsentByNameWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link IsTaskAbsentByIdWithoutUserId }
     * 
     */
    public IsTaskAbsentByIdWithoutUserId createIsTaskAbsentByIdWithoutUserId() {
        return new IsTaskAbsentByIdWithoutUserId();
    }

    /**
     * Create an instance of {@link IsTaskAbsentByIdWithoutUserIdResponse }
     * 
     */
    public IsTaskAbsentByIdWithoutUserIdResponse createIsTaskAbsentByIdWithoutUserIdResponse() {
        return new IsTaskAbsentByIdWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link IsTaskAbsentByIndexWithoutUserId }
     * 
     */
    public IsTaskAbsentByIndexWithoutUserId createIsTaskAbsentByIndexWithoutUserId() {
        return new IsTaskAbsentByIndexWithoutUserId();
    }

    /**
     * Create an instance of {@link IsTaskAbsentByIndexWithoutUserIdResponse }
     * 
     */
    public IsTaskAbsentByIndexWithoutUserIdResponse createIsTaskAbsentByIndexWithoutUserIdResponse() {
        return new IsTaskAbsentByIndexWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link IsTaskAbsentByNameWithoutUserId }
     * 
     */
    public IsTaskAbsentByNameWithoutUserId createIsTaskAbsentByNameWithoutUserId() {
        return new IsTaskAbsentByNameWithoutUserId();
    }

    /**
     * Create an instance of {@link IsTaskAbsentByNameWithoutUserIdResponse }
     * 
     */
    public IsTaskAbsentByNameWithoutUserIdResponse createIsTaskAbsentByNameWithoutUserIdResponse() {
        return new IsTaskAbsentByNameWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link IsUserAbsentById }
     * 
     */
    public IsUserAbsentById createIsUserAbsentById() {
        return new IsUserAbsentById();
    }

    /**
     * Create an instance of {@link IsUserAbsentByIdResponse }
     * 
     */
    public IsUserAbsentByIdResponse createIsUserAbsentByIdResponse() {
        return new IsUserAbsentByIdResponse();
    }

    /**
     * Create an instance of {@link IsUserAbsentByIndex }
     * 
     */
    public IsUserAbsentByIndex createIsUserAbsentByIndex() {
        return new IsUserAbsentByIndex();
    }

    /**
     * Create an instance of {@link IsUserAbsentByIndexResponse }
     * 
     */
    public IsUserAbsentByIndexResponse createIsUserAbsentByIndexResponse() {
        return new IsUserAbsentByIndexResponse();
    }

    /**
     * Create an instance of {@link IsUserLoginExists }
     * 
     */
    public IsUserLoginExists createIsUserLoginExists() {
        return new IsUserLoginExists();
    }

    /**
     * Create an instance of {@link IsUserLoginExistsResponse }
     * 
     */
    public IsUserLoginExistsResponse createIsUserLoginExistsResponse() {
        return new IsUserLoginExistsResponse();
    }

    /**
     * Create an instance of {@link LoadBackup }
     * 
     */
    public LoadBackup createLoadBackup() {
        return new LoadBackup();
    }

    /**
     * Create an instance of {@link LoadBackupResponse }
     * 
     */
    public LoadBackupResponse createLoadBackupResponse() {
        return new LoadBackupResponse();
    }

    /**
     * Create an instance of {@link LoadBase64Data }
     * 
     */
    public LoadBase64Data createLoadBase64Data() {
        return new LoadBase64Data();
    }

    /**
     * Create an instance of {@link LoadBase64DataResponse }
     * 
     */
    public LoadBase64DataResponse createLoadBase64DataResponse() {
        return new LoadBase64DataResponse();
    }

    /**
     * Create an instance of {@link LoadBinaryData }
     * 
     */
    public LoadBinaryData createLoadBinaryData() {
        return new LoadBinaryData();
    }

    /**
     * Create an instance of {@link LoadBinaryDataResponse }
     * 
     */
    public LoadBinaryDataResponse createLoadBinaryDataResponse() {
        return new LoadBinaryDataResponse();
    }

    /**
     * Create an instance of {@link LoadJsonFasterXmlData }
     * 
     */
    public LoadJsonFasterXmlData createLoadJsonFasterXmlData() {
        return new LoadJsonFasterXmlData();
    }

    /**
     * Create an instance of {@link LoadJsonFasterXmlDataResponse }
     * 
     */
    public LoadJsonFasterXmlDataResponse createLoadJsonFasterXmlDataResponse() {
        return new LoadJsonFasterXmlDataResponse();
    }

    /**
     * Create an instance of {@link LoadJsonJaxBData }
     * 
     */
    public LoadJsonJaxBData createLoadJsonJaxBData() {
        return new LoadJsonJaxBData();
    }

    /**
     * Create an instance of {@link LoadJsonJaxBDataResponse }
     * 
     */
    public LoadJsonJaxBDataResponse createLoadJsonJaxBDataResponse() {
        return new LoadJsonJaxBDataResponse();
    }

    /**
     * Create an instance of {@link LoadXmlFasterXmlData }
     * 
     */
    public LoadXmlFasterXmlData createLoadXmlFasterXmlData() {
        return new LoadXmlFasterXmlData();
    }

    /**
     * Create an instance of {@link LoadXmlFasterXmlDataResponse }
     * 
     */
    public LoadXmlFasterXmlDataResponse createLoadXmlFasterXmlDataResponse() {
        return new LoadXmlFasterXmlDataResponse();
    }

    /**
     * Create an instance of {@link LoadXmlJaxBData }
     * 
     */
    public LoadXmlJaxBData createLoadXmlJaxBData() {
        return new LoadXmlJaxBData();
    }

    /**
     * Create an instance of {@link LoadXmlJaxBDataResponse }
     * 
     */
    public LoadXmlJaxBDataResponse createLoadXmlJaxBDataResponse() {
        return new LoadXmlJaxBDataResponse();
    }

    /**
     * Create an instance of {@link LoadYamlData }
     * 
     */
    public LoadYamlData createLoadYamlData() {
        return new LoadYamlData();
    }

    /**
     * Create an instance of {@link LoadYamlDataResponse }
     * 
     */
    public LoadYamlDataResponse createLoadYamlDataResponse() {
        return new LoadYamlDataResponse();
    }

    /**
     * Create an instance of {@link LockUserByEmail }
     * 
     */
    public LockUserByEmail createLockUserByEmail() {
        return new LockUserByEmail();
    }

    /**
     * Create an instance of {@link LockUserByEmailResponse }
     * 
     */
    public LockUserByEmailResponse createLockUserByEmailResponse() {
        return new LockUserByEmailResponse();
    }

    /**
     * Create an instance of {@link LockUserByLogin }
     * 
     */
    public LockUserByLogin createLockUserByLogin() {
        return new LockUserByLogin();
    }

    /**
     * Create an instance of {@link LockUserByLoginResponse }
     * 
     */
    public LockUserByLoginResponse createLockUserByLoginResponse() {
        return new LockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectByIdWithoutUserId }
     * 
     */
    public RemoveProjectByIdWithoutUserId createRemoveProjectByIdWithoutUserId() {
        return new RemoveProjectByIdWithoutUserId();
    }

    /**
     * Create an instance of {@link RemoveProjectByIdWithoutUserIdResponse }
     * 
     */
    public RemoveProjectByIdWithoutUserIdResponse createRemoveProjectByIdWithoutUserIdResponse() {
        return new RemoveProjectByIdWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectByIndexWithoutUserId }
     * 
     */
    public RemoveProjectByIndexWithoutUserId createRemoveProjectByIndexWithoutUserId() {
        return new RemoveProjectByIndexWithoutUserId();
    }

    /**
     * Create an instance of {@link RemoveProjectByIndexWithoutUserIdResponse }
     * 
     */
    public RemoveProjectByIndexWithoutUserIdResponse createRemoveProjectByIndexWithoutUserIdResponse() {
        return new RemoveProjectByIndexWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectWithoutUserId }
     * 
     */
    public RemoveProjectWithoutUserId createRemoveProjectWithoutUserId() {
        return new RemoveProjectWithoutUserId();
    }

    /**
     * Create an instance of {@link RemoveProjectWithoutUserIdResponse }
     * 
     */
    public RemoveProjectWithoutUserIdResponse createRemoveProjectWithoutUserIdResponse() {
        return new RemoveProjectWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskByIdWithoutUserId }
     * 
     */
    public RemoveTaskByIdWithoutUserId createRemoveTaskByIdWithoutUserId() {
        return new RemoveTaskByIdWithoutUserId();
    }

    /**
     * Create an instance of {@link RemoveTaskByIdWithoutUserIdResponse }
     * 
     */
    public RemoveTaskByIdWithoutUserIdResponse createRemoveTaskByIdWithoutUserIdResponse() {
        return new RemoveTaskByIdWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskWithoutUserId }
     * 
     */
    public RemoveTaskWithoutUserId createRemoveTaskWithoutUserId() {
        return new RemoveTaskWithoutUserId();
    }

    /**
     * Create an instance of {@link RemoveTaskWithoutUserIdResponse }
     * 
     */
    public RemoveTaskWithoutUserIdResponse createRemoveTaskWithoutUserIdResponse() {
        return new RemoveTaskWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserByEmail }
     * 
     */
    public RemoveUserByEmail createRemoveUserByEmail() {
        return new RemoveUserByEmail();
    }

    /**
     * Create an instance of {@link RemoveUserByEmailResponse }
     * 
     */
    public RemoveUserByEmailResponse createRemoveUserByEmailResponse() {
        return new RemoveUserByEmailResponse();
    }

    /**
     * Create an instance of {@link RemoveUserById }
     * 
     */
    public RemoveUserById createRemoveUserById() {
        return new RemoveUserById();
    }

    /**
     * Create an instance of {@link RemoveUserByIdResponse }
     * 
     */
    public RemoveUserByIdResponse createRemoveUserByIdResponse() {
        return new RemoveUserByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUserByIndex }
     * 
     */
    public RemoveUserByIndex createRemoveUserByIndex() {
        return new RemoveUserByIndex();
    }

    /**
     * Create an instance of {@link RemoveUserByIndexResponse }
     * 
     */
    public RemoveUserByIndexResponse createRemoveUserByIndexResponse() {
        return new RemoveUserByIndexResponse();
    }

    /**
     * Create an instance of {@link RemoveUserByLogin }
     * 
     */
    public RemoveUserByLogin createRemoveUserByLogin() {
        return new RemoveUserByLogin();
    }

    /**
     * Create an instance of {@link RemoveUserByLoginResponse }
     * 
     */
    public RemoveUserByLoginResponse createRemoveUserByLoginResponse() {
        return new RemoveUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link SaveBackup }
     * 
     */
    public SaveBackup createSaveBackup() {
        return new SaveBackup();
    }

    /**
     * Create an instance of {@link SaveBackupResponse }
     * 
     */
    public SaveBackupResponse createSaveBackupResponse() {
        return new SaveBackupResponse();
    }

    /**
     * Create an instance of {@link SaveBase64Data }
     * 
     */
    public SaveBase64Data createSaveBase64Data() {
        return new SaveBase64Data();
    }

    /**
     * Create an instance of {@link SaveBase64DataResponse }
     * 
     */
    public SaveBase64DataResponse createSaveBase64DataResponse() {
        return new SaveBase64DataResponse();
    }

    /**
     * Create an instance of {@link SaveBinaryData }
     * 
     */
    public SaveBinaryData createSaveBinaryData() {
        return new SaveBinaryData();
    }

    /**
     * Create an instance of {@link SaveBinaryDataResponse }
     * 
     */
    public SaveBinaryDataResponse createSaveBinaryDataResponse() {
        return new SaveBinaryDataResponse();
    }

    /**
     * Create an instance of {@link SaveJsonFasterXmlData }
     * 
     */
    public SaveJsonFasterXmlData createSaveJsonFasterXmlData() {
        return new SaveJsonFasterXmlData();
    }

    /**
     * Create an instance of {@link SaveJsonFasterXmlDataResponse }
     * 
     */
    public SaveJsonFasterXmlDataResponse createSaveJsonFasterXmlDataResponse() {
        return new SaveJsonFasterXmlDataResponse();
    }

    /**
     * Create an instance of {@link SaveJsonJaxBData }
     * 
     */
    public SaveJsonJaxBData createSaveJsonJaxBData() {
        return new SaveJsonJaxBData();
    }

    /**
     * Create an instance of {@link SaveJsonJaxBDataResponse }
     * 
     */
    public SaveJsonJaxBDataResponse createSaveJsonJaxBDataResponse() {
        return new SaveJsonJaxBDataResponse();
    }

    /**
     * Create an instance of {@link SaveXmlFasterXmlData }
     * 
     */
    public SaveXmlFasterXmlData createSaveXmlFasterXmlData() {
        return new SaveXmlFasterXmlData();
    }

    /**
     * Create an instance of {@link SaveXmlFasterXmlDataResponse }
     * 
     */
    public SaveXmlFasterXmlDataResponse createSaveXmlFasterXmlDataResponse() {
        return new SaveXmlFasterXmlDataResponse();
    }

    /**
     * Create an instance of {@link SaveXmlJaxBData }
     * 
     */
    public SaveXmlJaxBData createSaveXmlJaxBData() {
        return new SaveXmlJaxBData();
    }

    /**
     * Create an instance of {@link SaveXmlJaxBDataResponse }
     * 
     */
    public SaveXmlJaxBDataResponse createSaveXmlJaxBDataResponse() {
        return new SaveXmlJaxBDataResponse();
    }

    /**
     * Create an instance of {@link SaveYamlData }
     * 
     */
    public SaveYamlData createSaveYamlData() {
        return new SaveYamlData();
    }

    /**
     * Create an instance of {@link SaveYamlDataResponse }
     * 
     */
    public SaveYamlDataResponse createSaveYamlDataResponse() {
        return new SaveYamlDataResponse();
    }

    /**
     * Create an instance of {@link ShowAllUserWithProject }
     * 
     */
    public ShowAllUserWithProject createShowAllUserWithProject() {
        return new ShowAllUserWithProject();
    }

    /**
     * Create an instance of {@link ShowAllUserWithProjectResponse }
     * 
     */
    public ShowAllUserWithProjectResponse createShowAllUserWithProjectResponse() {
        return new ShowAllUserWithProjectResponse();
    }

    /**
     * Create an instance of {@link ShowUserList }
     * 
     */
    public ShowUserList createShowUserList() {
        return new ShowUserList();
    }

    /**
     * Create an instance of {@link ShowUserListResponse }
     * 
     */
    public ShowUserListResponse createShowUserListResponse() {
        return new ShowUserListResponse();
    }

    /**
     * Create an instance of {@link SizeProjectWithoutUserId }
     * 
     */
    public SizeProjectWithoutUserId createSizeProjectWithoutUserId() {
        return new SizeProjectWithoutUserId();
    }

    /**
     * Create an instance of {@link SizeProjectWithoutUserIdResponse }
     * 
     */
    public SizeProjectWithoutUserIdResponse createSizeProjectWithoutUserIdResponse() {
        return new SizeProjectWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link SizeTaskWithoutUserId }
     * 
     */
    public SizeTaskWithoutUserId createSizeTaskWithoutUserId() {
        return new SizeTaskWithoutUserId();
    }

    /**
     * Create an instance of {@link SizeTaskWithoutUserIdResponse }
     * 
     */
    public SizeTaskWithoutUserIdResponse createSizeTaskWithoutUserIdResponse() {
        return new SizeTaskWithoutUserIdResponse();
    }

    /**
     * Create an instance of {@link SizeUser }
     * 
     */
    public SizeUser createSizeUser() {
        return new SizeUser();
    }

    /**
     * Create an instance of {@link SizeUserResponse }
     * 
     */
    public SizeUserResponse createSizeUserResponse() {
        return new SizeUserResponse();
    }

    /**
     * Create an instance of {@link UnlockUserByEmail }
     * 
     */
    public UnlockUserByEmail createUnlockUserByEmail() {
        return new UnlockUserByEmail();
    }

    /**
     * Create an instance of {@link UnlockUserByEmailResponse }
     * 
     */
    public UnlockUserByEmailResponse createUnlockUserByEmailResponse() {
        return new UnlockUserByEmailResponse();
    }

    /**
     * Create an instance of {@link UnlockUserByLogin }
     * 
     */
    public UnlockUserByLogin createUnlockUserByLogin() {
        return new UnlockUserByLogin();
    }

    /**
     * Create an instance of {@link UnlockUserByLoginResponse }
     * 
     */
    public UnlockUserByLoginResponse createUnlockUserByLoginResponse() {
        return new UnlockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProjectAllWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddProjectAllWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addProjectAllWithoutUserId")
    public JAXBElement<AddProjectAllWithoutUserId> createAddProjectAllWithoutUserId(AddProjectAllWithoutUserId value) {
        return new JAXBElement<AddProjectAllWithoutUserId>(_AddProjectAllWithoutUserId_QNAME, AddProjectAllWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProjectAllWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddProjectAllWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addProjectAllWithoutUserIdResponse")
    public JAXBElement<AddProjectAllWithoutUserIdResponse> createAddProjectAllWithoutUserIdResponse(AddProjectAllWithoutUserIdResponse value) {
        return new JAXBElement<AddProjectAllWithoutUserIdResponse>(_AddProjectAllWithoutUserIdResponse_QNAME, AddProjectAllWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProjectWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddProjectWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addProjectWithoutUserId")
    public JAXBElement<AddProjectWithoutUserId> createAddProjectWithoutUserId(AddProjectWithoutUserId value) {
        return new JAXBElement<AddProjectWithoutUserId>(_AddProjectWithoutUserId_QNAME, AddProjectWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddProjectWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddProjectWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addProjectWithoutUserIdResponse")
    public JAXBElement<AddProjectWithoutUserIdResponse> createAddProjectWithoutUserIdResponse(AddProjectWithoutUserIdResponse value) {
        return new JAXBElement<AddProjectWithoutUserIdResponse>(_AddProjectWithoutUserIdResponse_QNAME, AddProjectWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskAllWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddTaskAllWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addTaskAllWithoutUserId")
    public JAXBElement<AddTaskAllWithoutUserId> createAddTaskAllWithoutUserId(AddTaskAllWithoutUserId value) {
        return new JAXBElement<AddTaskAllWithoutUserId>(_AddTaskAllWithoutUserId_QNAME, AddTaskAllWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskAllWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddTaskAllWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addTaskAllWithoutUserIdResponse")
    public JAXBElement<AddTaskAllWithoutUserIdResponse> createAddTaskAllWithoutUserIdResponse(AddTaskAllWithoutUserIdResponse value) {
        return new JAXBElement<AddTaskAllWithoutUserIdResponse>(_AddTaskAllWithoutUserIdResponse_QNAME, AddTaskAllWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddTaskWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addTaskWithoutUserId")
    public JAXBElement<AddTaskWithoutUserId> createAddTaskWithoutUserId(AddTaskWithoutUserId value) {
        return new JAXBElement<AddTaskWithoutUserId>(_AddTaskWithoutUserId_QNAME, AddTaskWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddTaskWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addTaskWithoutUserIdResponse")
    public JAXBElement<AddTaskWithoutUserIdResponse> createAddTaskWithoutUserIdResponse(AddTaskWithoutUserIdResponse value) {
        return new JAXBElement<AddTaskWithoutUserIdResponse>(_AddTaskWithoutUserIdResponse_QNAME, AddTaskWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addUser")
    public JAXBElement<AddUser> createAddUser(AddUser value) {
        return new JAXBElement<AddUser>(_AddUser_QNAME, AddUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddUserAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addUserAll")
    public JAXBElement<AddUserAll> createAddUserAll(AddUserAll value) {
        return new JAXBElement<AddUserAll>(_AddUserAll_QNAME, AddUserAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddUserAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addUserAllResponse")
    public JAXBElement<AddUserAllResponse> createAddUserAllResponse(AddUserAllResponse value) {
        return new JAXBElement<AddUserAllResponse>(_AddUserAllResponse_QNAME, AddUserAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "addUserResponse")
    public JAXBElement<AddUserResponse> createAddUserResponse(AddUserResponse value) {
        return new JAXBElement<AddUserResponse>(_AddUserResponse_QNAME, AddUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearBackup }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearBackup }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearBackup")
    public JAXBElement<ClearBackup> createClearBackup(ClearBackup value) {
        return new JAXBElement<ClearBackup>(_ClearBackup_QNAME, ClearBackup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearBackupResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearBackupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearBackupResponse")
    public JAXBElement<ClearBackupResponse> createClearBackupResponse(ClearBackupResponse value) {
        return new JAXBElement<ClearBackupResponse>(_ClearBackupResponse_QNAME, ClearBackupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearProjectWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearProjectWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearProjectWithoutUserId")
    public JAXBElement<ClearProjectWithoutUserId> createClearProjectWithoutUserId(ClearProjectWithoutUserId value) {
        return new JAXBElement<ClearProjectWithoutUserId>(_ClearProjectWithoutUserId_QNAME, ClearProjectWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearProjectWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearProjectWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearProjectWithoutUserIdResponse")
    public JAXBElement<ClearProjectWithoutUserIdResponse> createClearProjectWithoutUserIdResponse(ClearProjectWithoutUserIdResponse value) {
        return new JAXBElement<ClearProjectWithoutUserIdResponse>(_ClearProjectWithoutUserIdResponse_QNAME, ClearProjectWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTaskWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTaskWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearTaskWithoutUserId")
    public JAXBElement<ClearTaskWithoutUserId> createClearTaskWithoutUserId(ClearTaskWithoutUserId value) {
        return new JAXBElement<ClearTaskWithoutUserId>(_ClearTaskWithoutUserId_QNAME, ClearTaskWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearTaskWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearTaskWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearTaskWithoutUserIdResponse")
    public JAXBElement<ClearTaskWithoutUserIdResponse> createClearTaskWithoutUserIdResponse(ClearTaskWithoutUserIdResponse value) {
        return new JAXBElement<ClearTaskWithoutUserIdResponse>(_ClearTaskWithoutUserIdResponse_QNAME, ClearTaskWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearUser")
    public JAXBElement<ClearUser> createClearUser(ClearUser value) {
        return new JAXBElement<ClearUser>(_ClearUser_QNAME, ClearUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "clearUserResponse")
    public JAXBElement<ClearUserResponse> createClearUserResponse(ClearUserResponse value) {
        return new JAXBElement<ClearUserResponse>(_ClearUserResponse_QNAME, ClearUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUserWithAll")
    public JAXBElement<CreateUserWithAll> createCreateUserWithAll(CreateUserWithAll value) {
        return new JAXBElement<CreateUserWithAll>(_CreateUserWithAll_QNAME, CreateUserWithAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUserWithAllResponse")
    public JAXBElement<CreateUserWithAllResponse> createCreateUserWithAllResponse(CreateUserWithAllResponse value) {
        return new JAXBElement<CreateUserWithAllResponse>(_CreateUserWithAllResponse_QNAME, CreateUserWithAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmail }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUserWithEmail")
    public JAXBElement<CreateUserWithEmail> createCreateUserWithEmail(CreateUserWithEmail value) {
        return new JAXBElement<CreateUserWithEmail>(_CreateUserWithEmail_QNAME, CreateUserWithEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUserWithEmailResponse")
    public JAXBElement<CreateUserWithEmailResponse> createCreateUserWithEmailResponse(CreateUserWithEmailResponse value) {
        return new JAXBElement<CreateUserWithEmailResponse>(_CreateUserWithEmailResponse_QNAME, CreateUserWithEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRole }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithRole }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUserWithRole")
    public JAXBElement<CreateUserWithRole> createCreateUserWithRole(CreateUserWithRole value) {
        return new JAXBElement<CreateUserWithRole>(_CreateUserWithRole_QNAME, CreateUserWithRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRoleResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserWithRoleResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "createUserWithRoleResponse")
    public JAXBElement<CreateUserWithRoleResponse> createCreateUserWithRoleResponse(CreateUserWithRoleResponse value) {
        return new JAXBElement<CreateUserWithRoleResponse>(_CreateUserWithRoleResponse_QNAME, CreateUserWithRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectAllWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectAllWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectAllWithoutUserId")
    public JAXBElement<FindProjectAllWithoutUserId> createFindProjectAllWithoutUserId(FindProjectAllWithoutUserId value) {
        return new JAXBElement<FindProjectAllWithoutUserId>(_FindProjectAllWithoutUserId_QNAME, FindProjectAllWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectAllWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectAllWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectAllWithoutUserIdResponse")
    public JAXBElement<FindProjectAllWithoutUserIdResponse> createFindProjectAllWithoutUserIdResponse(FindProjectAllWithoutUserIdResponse value) {
        return new JAXBElement<FindProjectAllWithoutUserIdResponse>(_FindProjectAllWithoutUserIdResponse_QNAME, FindProjectAllWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIdWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectByIdWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectByIdWithoutUserId")
    public JAXBElement<FindProjectByIdWithoutUserId> createFindProjectByIdWithoutUserId(FindProjectByIdWithoutUserId value) {
        return new JAXBElement<FindProjectByIdWithoutUserId>(_FindProjectByIdWithoutUserId_QNAME, FindProjectByIdWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIdWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectByIdWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectByIdWithoutUserIdResponse")
    public JAXBElement<FindProjectByIdWithoutUserIdResponse> createFindProjectByIdWithoutUserIdResponse(FindProjectByIdWithoutUserIdResponse value) {
        return new JAXBElement<FindProjectByIdWithoutUserIdResponse>(_FindProjectByIdWithoutUserIdResponse_QNAME, FindProjectByIdWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIndexWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectByIndexWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectByIndexWithoutUserId")
    public JAXBElement<FindProjectByIndexWithoutUserId> createFindProjectByIndexWithoutUserId(FindProjectByIndexWithoutUserId value) {
        return new JAXBElement<FindProjectByIndexWithoutUserId>(_FindProjectByIndexWithoutUserId_QNAME, FindProjectByIndexWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIndexWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectByIndexWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectByIndexWithoutUserIdResponse")
    public JAXBElement<FindProjectByIndexWithoutUserIdResponse> createFindProjectByIndexWithoutUserIdResponse(FindProjectByIndexWithoutUserIdResponse value) {
        return new JAXBElement<FindProjectByIndexWithoutUserIdResponse>(_FindProjectByIndexWithoutUserIdResponse_QNAME, FindProjectByIndexWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByNameWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectByNameWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectByNameWithoutUserId")
    public JAXBElement<FindProjectByNameWithoutUserId> createFindProjectByNameWithoutUserId(FindProjectByNameWithoutUserId value) {
        return new JAXBElement<FindProjectByNameWithoutUserId>(_FindProjectByNameWithoutUserId_QNAME, FindProjectByNameWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByNameWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindProjectByNameWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findProjectByNameWithoutUserIdResponse")
    public JAXBElement<FindProjectByNameWithoutUserIdResponse> createFindProjectByNameWithoutUserIdResponse(FindProjectByNameWithoutUserIdResponse value) {
        return new JAXBElement<FindProjectByNameWithoutUserIdResponse>(_FindProjectByNameWithoutUserIdResponse_QNAME, FindProjectByNameWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskAllWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskAllWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskAllWithoutUserId")
    public JAXBElement<FindTaskAllWithoutUserId> createFindTaskAllWithoutUserId(FindTaskAllWithoutUserId value) {
        return new JAXBElement<FindTaskAllWithoutUserId>(_FindTaskAllWithoutUserId_QNAME, FindTaskAllWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskAllWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskAllWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskAllWithoutUserIdResponse")
    public JAXBElement<FindTaskAllWithoutUserIdResponse> createFindTaskAllWithoutUserIdResponse(FindTaskAllWithoutUserIdResponse value) {
        return new JAXBElement<FindTaskAllWithoutUserIdResponse>(_FindTaskAllWithoutUserIdResponse_QNAME, FindTaskAllWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIdWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByIdWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskByIdWithoutUserId")
    public JAXBElement<FindTaskByIdWithoutUserId> createFindTaskByIdWithoutUserId(FindTaskByIdWithoutUserId value) {
        return new JAXBElement<FindTaskByIdWithoutUserId>(_FindTaskByIdWithoutUserId_QNAME, FindTaskByIdWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIdWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByIdWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskByIdWithoutUserIdResponse")
    public JAXBElement<FindTaskByIdWithoutUserIdResponse> createFindTaskByIdWithoutUserIdResponse(FindTaskByIdWithoutUserIdResponse value) {
        return new JAXBElement<FindTaskByIdWithoutUserIdResponse>(_FindTaskByIdWithoutUserIdResponse_QNAME, FindTaskByIdWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIndexWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByIndexWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskByIndexWithoutUserId")
    public JAXBElement<FindTaskByIndexWithoutUserId> createFindTaskByIndexWithoutUserId(FindTaskByIndexWithoutUserId value) {
        return new JAXBElement<FindTaskByIndexWithoutUserId>(_FindTaskByIndexWithoutUserId_QNAME, FindTaskByIndexWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByIndexWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByIndexWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskByIndexWithoutUserIdResponse")
    public JAXBElement<FindTaskByIndexWithoutUserIdResponse> createFindTaskByIndexWithoutUserIdResponse(FindTaskByIndexWithoutUserIdResponse value) {
        return new JAXBElement<FindTaskByIndexWithoutUserIdResponse>(_FindTaskByIndexWithoutUserIdResponse_QNAME, FindTaskByIndexWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByNameWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByNameWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskByNameWithoutUserId")
    public JAXBElement<FindTaskByNameWithoutUserId> createFindTaskByNameWithoutUserId(FindTaskByNameWithoutUserId value) {
        return new JAXBElement<FindTaskByNameWithoutUserId>(_FindTaskByNameWithoutUserId_QNAME, FindTaskByNameWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindTaskByNameWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindTaskByNameWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findTaskByNameWithoutUserIdResponse")
    public JAXBElement<FindTaskByNameWithoutUserIdResponse> createFindTaskByNameWithoutUserIdResponse(FindTaskByNameWithoutUserIdResponse value) {
        return new JAXBElement<FindTaskByNameWithoutUserIdResponse>(_FindTaskByNameWithoutUserIdResponse_QNAME, FindTaskByNameWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserAll")
    public JAXBElement<FindUserAll> createFindUserAll(FindUserAll value) {
        return new JAXBElement<FindUserAll>(_FindUserAll_QNAME, FindUserAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserAllResponse")
    public JAXBElement<FindUserAllResponse> createFindUserAllResponse(FindUserAllResponse value) {
        return new JAXBElement<FindUserAllResponse>(_FindUserAllResponse_QNAME, FindUserAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByEmail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByEmail }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserByEmail")
    public JAXBElement<FindUserByEmail> createFindUserByEmail(FindUserByEmail value) {
        return new JAXBElement<FindUserByEmail>(_FindUserByEmail_QNAME, FindUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByEmailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByEmailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserByEmailResponse")
    public JAXBElement<FindUserByEmailResponse> createFindUserByEmailResponse(FindUserByEmailResponse value) {
        return new JAXBElement<FindUserByEmailResponse>(_FindUserByEmailResponse_QNAME, FindUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserByIndex")
    public JAXBElement<FindUserByIndex> createFindUserByIndex(FindUserByIndex value) {
        return new JAXBElement<FindUserByIndex>(_FindUserByIndex_QNAME, FindUserByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserByIndexResponse")
    public JAXBElement<FindUserByIndexResponse> createFindUserByIndexResponse(FindUserByIndexResponse value) {
        return new JAXBElement<FindUserByIndexResponse>(_FindUserByIndexResponse_QNAME, FindUserByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserByLogin")
    public JAXBElement<FindUserByLogin> createFindUserByLogin(FindUserByLogin value) {
        return new JAXBElement<FindUserByLogin>(_FindUserByLogin_QNAME, FindUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "findUserByLoginResponse")
    public JAXBElement<FindUserByLoginResponse> createFindUserByLoginResponse(FindUserByLoginResponse value) {
        return new JAXBElement<FindUserByLoginResponse>(_FindUserByLoginResponse_QNAME, FindUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectIdByIndexWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectIdByIndexWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getProjectIdByIndexWithoutUserId")
    public JAXBElement<GetProjectIdByIndexWithoutUserId> createGetProjectIdByIndexWithoutUserId(GetProjectIdByIndexWithoutUserId value) {
        return new JAXBElement<GetProjectIdByIndexWithoutUserId>(_GetProjectIdByIndexWithoutUserId_QNAME, GetProjectIdByIndexWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectIdByIndexWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectIdByIndexWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getProjectIdByIndexWithoutUserIdResponse")
    public JAXBElement<GetProjectIdByIndexWithoutUserIdResponse> createGetProjectIdByIndexWithoutUserIdResponse(GetProjectIdByIndexWithoutUserIdResponse value) {
        return new JAXBElement<GetProjectIdByIndexWithoutUserIdResponse>(_GetProjectIdByIndexWithoutUserIdResponse_QNAME, GetProjectIdByIndexWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectIdByNameWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectIdByNameWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getProjectIdByNameWithoutUserId")
    public JAXBElement<GetProjectIdByNameWithoutUserId> createGetProjectIdByNameWithoutUserId(GetProjectIdByNameWithoutUserId value) {
        return new JAXBElement<GetProjectIdByNameWithoutUserId>(_GetProjectIdByNameWithoutUserId_QNAME, GetProjectIdByNameWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectIdByNameWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectIdByNameWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getProjectIdByNameWithoutUserIdResponse")
    public JAXBElement<GetProjectIdByNameWithoutUserIdResponse> createGetProjectIdByNameWithoutUserIdResponse(GetProjectIdByNameWithoutUserIdResponse value) {
        return new JAXBElement<GetProjectIdByNameWithoutUserIdResponse>(_GetProjectIdByNameWithoutUserIdResponse_QNAME, GetProjectIdByNameWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskIdByIndexWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskIdByIndexWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getTaskIdByIndexWithoutUserId")
    public JAXBElement<GetTaskIdByIndexWithoutUserId> createGetTaskIdByIndexWithoutUserId(GetTaskIdByIndexWithoutUserId value) {
        return new JAXBElement<GetTaskIdByIndexWithoutUserId>(_GetTaskIdByIndexWithoutUserId_QNAME, GetTaskIdByIndexWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskIdByIndexWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskIdByIndexWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getTaskIdByIndexWithoutUserIdResponse")
    public JAXBElement<GetTaskIdByIndexWithoutUserIdResponse> createGetTaskIdByIndexWithoutUserIdResponse(GetTaskIdByIndexWithoutUserIdResponse value) {
        return new JAXBElement<GetTaskIdByIndexWithoutUserIdResponse>(_GetTaskIdByIndexWithoutUserIdResponse_QNAME, GetTaskIdByIndexWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskIdByNameWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskIdByNameWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getTaskIdByNameWithoutUserId")
    public JAXBElement<GetTaskIdByNameWithoutUserId> createGetTaskIdByNameWithoutUserId(GetTaskIdByNameWithoutUserId value) {
        return new JAXBElement<GetTaskIdByNameWithoutUserId>(_GetTaskIdByNameWithoutUserId_QNAME, GetTaskIdByNameWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskIdByNameWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskIdByNameWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getTaskIdByNameWithoutUserIdResponse")
    public JAXBElement<GetTaskIdByNameWithoutUserIdResponse> createGetTaskIdByNameWithoutUserIdResponse(GetTaskIdByNameWithoutUserIdResponse value) {
        return new JAXBElement<GetTaskIdByNameWithoutUserIdResponse>(_GetTaskIdByNameWithoutUserIdResponse_QNAME, GetTaskIdByNameWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserIdByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserIdByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getUserIdByIndex")
    public JAXBElement<GetUserIdByIndex> createGetUserIdByIndex(GetUserIdByIndex value) {
        return new JAXBElement<GetUserIdByIndex>(_GetUserIdByIndex_QNAME, GetUserIdByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserIdByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserIdByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "getUserIdByIndexResponse")
    public JAXBElement<GetUserIdByIndexResponse> createGetUserIdByIndexResponse(GetUserIdByIndexResponse value) {
        return new JAXBElement<GetUserIdByIndexResponse>(_GetUserIdByIndexResponse_QNAME, GetUserIdByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsEmailExists }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsEmailExists }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isEmailExists")
    public JAXBElement<IsEmailExists> createIsEmailExists(IsEmailExists value) {
        return new JAXBElement<IsEmailExists>(_IsEmailExists_QNAME, IsEmailExists.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsEmailExistsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsEmailExistsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isEmailExistsResponse")
    public JAXBElement<IsEmailExistsResponse> createIsEmailExistsResponse(IsEmailExistsResponse value) {
        return new JAXBElement<IsEmailExistsResponse>(_IsEmailExistsResponse_QNAME, IsEmailExistsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIdWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIdWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isProjectAbsentByIdWithoutUserId")
    public JAXBElement<IsProjectAbsentByIdWithoutUserId> createIsProjectAbsentByIdWithoutUserId(IsProjectAbsentByIdWithoutUserId value) {
        return new JAXBElement<IsProjectAbsentByIdWithoutUserId>(_IsProjectAbsentByIdWithoutUserId_QNAME, IsProjectAbsentByIdWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIdWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIdWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isProjectAbsentByIdWithoutUserIdResponse")
    public JAXBElement<IsProjectAbsentByIdWithoutUserIdResponse> createIsProjectAbsentByIdWithoutUserIdResponse(IsProjectAbsentByIdWithoutUserIdResponse value) {
        return new JAXBElement<IsProjectAbsentByIdWithoutUserIdResponse>(_IsProjectAbsentByIdWithoutUserIdResponse_QNAME, IsProjectAbsentByIdWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIndexWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIndexWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isProjectAbsentByIndexWithoutUserId")
    public JAXBElement<IsProjectAbsentByIndexWithoutUserId> createIsProjectAbsentByIndexWithoutUserId(IsProjectAbsentByIndexWithoutUserId value) {
        return new JAXBElement<IsProjectAbsentByIndexWithoutUserId>(_IsProjectAbsentByIndexWithoutUserId_QNAME, IsProjectAbsentByIndexWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIndexWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByIndexWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isProjectAbsentByIndexWithoutUserIdResponse")
    public JAXBElement<IsProjectAbsentByIndexWithoutUserIdResponse> createIsProjectAbsentByIndexWithoutUserIdResponse(IsProjectAbsentByIndexWithoutUserIdResponse value) {
        return new JAXBElement<IsProjectAbsentByIndexWithoutUserIdResponse>(_IsProjectAbsentByIndexWithoutUserIdResponse_QNAME, IsProjectAbsentByIndexWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByNameWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByNameWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isProjectAbsentByNameWithoutUserId")
    public JAXBElement<IsProjectAbsentByNameWithoutUserId> createIsProjectAbsentByNameWithoutUserId(IsProjectAbsentByNameWithoutUserId value) {
        return new JAXBElement<IsProjectAbsentByNameWithoutUserId>(_IsProjectAbsentByNameWithoutUserId_QNAME, IsProjectAbsentByNameWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByNameWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsProjectAbsentByNameWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isProjectAbsentByNameWithoutUserIdResponse")
    public JAXBElement<IsProjectAbsentByNameWithoutUserIdResponse> createIsProjectAbsentByNameWithoutUserIdResponse(IsProjectAbsentByNameWithoutUserIdResponse value) {
        return new JAXBElement<IsProjectAbsentByNameWithoutUserIdResponse>(_IsProjectAbsentByNameWithoutUserIdResponse_QNAME, IsProjectAbsentByNameWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIdWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIdWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isTaskAbsentByIdWithoutUserId")
    public JAXBElement<IsTaskAbsentByIdWithoutUserId> createIsTaskAbsentByIdWithoutUserId(IsTaskAbsentByIdWithoutUserId value) {
        return new JAXBElement<IsTaskAbsentByIdWithoutUserId>(_IsTaskAbsentByIdWithoutUserId_QNAME, IsTaskAbsentByIdWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIdWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIdWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isTaskAbsentByIdWithoutUserIdResponse")
    public JAXBElement<IsTaskAbsentByIdWithoutUserIdResponse> createIsTaskAbsentByIdWithoutUserIdResponse(IsTaskAbsentByIdWithoutUserIdResponse value) {
        return new JAXBElement<IsTaskAbsentByIdWithoutUserIdResponse>(_IsTaskAbsentByIdWithoutUserIdResponse_QNAME, IsTaskAbsentByIdWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIndexWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIndexWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isTaskAbsentByIndexWithoutUserId")
    public JAXBElement<IsTaskAbsentByIndexWithoutUserId> createIsTaskAbsentByIndexWithoutUserId(IsTaskAbsentByIndexWithoutUserId value) {
        return new JAXBElement<IsTaskAbsentByIndexWithoutUserId>(_IsTaskAbsentByIndexWithoutUserId_QNAME, IsTaskAbsentByIndexWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIndexWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByIndexWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isTaskAbsentByIndexWithoutUserIdResponse")
    public JAXBElement<IsTaskAbsentByIndexWithoutUserIdResponse> createIsTaskAbsentByIndexWithoutUserIdResponse(IsTaskAbsentByIndexWithoutUserIdResponse value) {
        return new JAXBElement<IsTaskAbsentByIndexWithoutUserIdResponse>(_IsTaskAbsentByIndexWithoutUserIdResponse_QNAME, IsTaskAbsentByIndexWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByNameWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByNameWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isTaskAbsentByNameWithoutUserId")
    public JAXBElement<IsTaskAbsentByNameWithoutUserId> createIsTaskAbsentByNameWithoutUserId(IsTaskAbsentByNameWithoutUserId value) {
        return new JAXBElement<IsTaskAbsentByNameWithoutUserId>(_IsTaskAbsentByNameWithoutUserId_QNAME, IsTaskAbsentByNameWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByNameWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsTaskAbsentByNameWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isTaskAbsentByNameWithoutUserIdResponse")
    public JAXBElement<IsTaskAbsentByNameWithoutUserIdResponse> createIsTaskAbsentByNameWithoutUserIdResponse(IsTaskAbsentByNameWithoutUserIdResponse value) {
        return new JAXBElement<IsTaskAbsentByNameWithoutUserIdResponse>(_IsTaskAbsentByNameWithoutUserIdResponse_QNAME, IsTaskAbsentByNameWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsUserAbsentById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsUserAbsentById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isUserAbsentById")
    public JAXBElement<IsUserAbsentById> createIsUserAbsentById(IsUserAbsentById value) {
        return new JAXBElement<IsUserAbsentById>(_IsUserAbsentById_QNAME, IsUserAbsentById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsUserAbsentByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsUserAbsentByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isUserAbsentByIdResponse")
    public JAXBElement<IsUserAbsentByIdResponse> createIsUserAbsentByIdResponse(IsUserAbsentByIdResponse value) {
        return new JAXBElement<IsUserAbsentByIdResponse>(_IsUserAbsentByIdResponse_QNAME, IsUserAbsentByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsUserAbsentByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsUserAbsentByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isUserAbsentByIndex")
    public JAXBElement<IsUserAbsentByIndex> createIsUserAbsentByIndex(IsUserAbsentByIndex value) {
        return new JAXBElement<IsUserAbsentByIndex>(_IsUserAbsentByIndex_QNAME, IsUserAbsentByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsUserAbsentByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsUserAbsentByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isUserAbsentByIndexResponse")
    public JAXBElement<IsUserAbsentByIndexResponse> createIsUserAbsentByIndexResponse(IsUserAbsentByIndexResponse value) {
        return new JAXBElement<IsUserAbsentByIndexResponse>(_IsUserAbsentByIndexResponse_QNAME, IsUserAbsentByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsUserLoginExists }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsUserLoginExists }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isUserLoginExists")
    public JAXBElement<IsUserLoginExists> createIsUserLoginExists(IsUserLoginExists value) {
        return new JAXBElement<IsUserLoginExists>(_IsUserLoginExists_QNAME, IsUserLoginExists.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsUserLoginExistsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsUserLoginExistsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "isUserLoginExistsResponse")
    public JAXBElement<IsUserLoginExistsResponse> createIsUserLoginExistsResponse(IsUserLoginExistsResponse value) {
        return new JAXBElement<IsUserLoginExistsResponse>(_IsUserLoginExistsResponse_QNAME, IsUserLoginExistsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBackup }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBackup }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadBackup")
    public JAXBElement<LoadBackup> createLoadBackup(LoadBackup value) {
        return new JAXBElement<LoadBackup>(_LoadBackup_QNAME, LoadBackup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBackupResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBackupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadBackupResponse")
    public JAXBElement<LoadBackupResponse> createLoadBackupResponse(LoadBackupResponse value) {
        return new JAXBElement<LoadBackupResponse>(_LoadBackupResponse_QNAME, LoadBackupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBase64Data }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBase64Data }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadBase64Data")
    public JAXBElement<LoadBase64Data> createLoadBase64Data(LoadBase64Data value) {
        return new JAXBElement<LoadBase64Data>(_LoadBase64Data_QNAME, LoadBase64Data.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBase64DataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBase64DataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadBase64DataResponse")
    public JAXBElement<LoadBase64DataResponse> createLoadBase64DataResponse(LoadBase64DataResponse value) {
        return new JAXBElement<LoadBase64DataResponse>(_LoadBase64DataResponse_QNAME, LoadBase64DataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinaryData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBinaryData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadBinaryData")
    public JAXBElement<LoadBinaryData> createLoadBinaryData(LoadBinaryData value) {
        return new JAXBElement<LoadBinaryData>(_LoadBinaryData_QNAME, LoadBinaryData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinaryDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBinaryDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadBinaryDataResponse")
    public JAXBElement<LoadBinaryDataResponse> createLoadBinaryDataResponse(LoadBinaryDataResponse value) {
        return new JAXBElement<LoadBinaryDataResponse>(_LoadBinaryDataResponse_QNAME, LoadBinaryDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonFasterXmlData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadJsonFasterXmlData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadJsonFasterXmlData")
    public JAXBElement<LoadJsonFasterXmlData> createLoadJsonFasterXmlData(LoadJsonFasterXmlData value) {
        return new JAXBElement<LoadJsonFasterXmlData>(_LoadJsonFasterXmlData_QNAME, LoadJsonFasterXmlData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonFasterXmlDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadJsonFasterXmlDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadJsonFasterXmlDataResponse")
    public JAXBElement<LoadJsonFasterXmlDataResponse> createLoadJsonFasterXmlDataResponse(LoadJsonFasterXmlDataResponse value) {
        return new JAXBElement<LoadJsonFasterXmlDataResponse>(_LoadJsonFasterXmlDataResponse_QNAME, LoadJsonFasterXmlDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonJaxBData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadJsonJaxBData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadJsonJaxBData")
    public JAXBElement<LoadJsonJaxBData> createLoadJsonJaxBData(LoadJsonJaxBData value) {
        return new JAXBElement<LoadJsonJaxBData>(_LoadJsonJaxBData_QNAME, LoadJsonJaxBData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonJaxBDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadJsonJaxBDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadJsonJaxBDataResponse")
    public JAXBElement<LoadJsonJaxBDataResponse> createLoadJsonJaxBDataResponse(LoadJsonJaxBDataResponse value) {
        return new JAXBElement<LoadJsonJaxBDataResponse>(_LoadJsonJaxBDataResponse_QNAME, LoadJsonJaxBDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlFasterXmlData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadXmlFasterXmlData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadXmlFasterXmlData")
    public JAXBElement<LoadXmlFasterXmlData> createLoadXmlFasterXmlData(LoadXmlFasterXmlData value) {
        return new JAXBElement<LoadXmlFasterXmlData>(_LoadXmlFasterXmlData_QNAME, LoadXmlFasterXmlData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlFasterXmlDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadXmlFasterXmlDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadXmlFasterXmlDataResponse")
    public JAXBElement<LoadXmlFasterXmlDataResponse> createLoadXmlFasterXmlDataResponse(LoadXmlFasterXmlDataResponse value) {
        return new JAXBElement<LoadXmlFasterXmlDataResponse>(_LoadXmlFasterXmlDataResponse_QNAME, LoadXmlFasterXmlDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlJaxBData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadXmlJaxBData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadXmlJaxBData")
    public JAXBElement<LoadXmlJaxBData> createLoadXmlJaxBData(LoadXmlJaxBData value) {
        return new JAXBElement<LoadXmlJaxBData>(_LoadXmlJaxBData_QNAME, LoadXmlJaxBData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlJaxBDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadXmlJaxBDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadXmlJaxBDataResponse")
    public JAXBElement<LoadXmlJaxBDataResponse> createLoadXmlJaxBDataResponse(LoadXmlJaxBDataResponse value) {
        return new JAXBElement<LoadXmlJaxBDataResponse>(_LoadXmlJaxBDataResponse_QNAME, LoadXmlJaxBDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadYamlData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadYamlData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadYamlData")
    public JAXBElement<LoadYamlData> createLoadYamlData(LoadYamlData value) {
        return new JAXBElement<LoadYamlData>(_LoadYamlData_QNAME, LoadYamlData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadYamlDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadYamlDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "loadYamlDataResponse")
    public JAXBElement<LoadYamlDataResponse> createLoadYamlDataResponse(LoadYamlDataResponse value) {
        return new JAXBElement<LoadYamlDataResponse>(_LoadYamlDataResponse_QNAME, LoadYamlDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByEmail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserByEmail }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "lockUserByEmail")
    public JAXBElement<LockUserByEmail> createLockUserByEmail(LockUserByEmail value) {
        return new JAXBElement<LockUserByEmail>(_LockUserByEmail_QNAME, LockUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByEmailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserByEmailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "lockUserByEmailResponse")
    public JAXBElement<LockUserByEmailResponse> createLockUserByEmailResponse(LockUserByEmailResponse value) {
        return new JAXBElement<LockUserByEmailResponse>(_LockUserByEmailResponse_QNAME, LockUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "lockUserByLogin")
    public JAXBElement<LockUserByLogin> createLockUserByLogin(LockUserByLogin value) {
        return new JAXBElement<LockUserByLogin>(_LockUserByLogin_QNAME, LockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "lockUserByLoginResponse")
    public JAXBElement<LockUserByLoginResponse> createLockUserByLoginResponse(LockUserByLoginResponse value) {
        return new JAXBElement<LockUserByLoginResponse>(_LockUserByLoginResponse_QNAME, LockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeProjectByIdWithoutUserId")
    public JAXBElement<RemoveProjectByIdWithoutUserId> createRemoveProjectByIdWithoutUserId(RemoveProjectByIdWithoutUserId value) {
        return new JAXBElement<RemoveProjectByIdWithoutUserId>(_RemoveProjectByIdWithoutUserId_QNAME, RemoveProjectByIdWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeProjectByIdWithoutUserIdResponse")
    public JAXBElement<RemoveProjectByIdWithoutUserIdResponse> createRemoveProjectByIdWithoutUserIdResponse(RemoveProjectByIdWithoutUserIdResponse value) {
        return new JAXBElement<RemoveProjectByIdWithoutUserIdResponse>(_RemoveProjectByIdWithoutUserIdResponse_QNAME, RemoveProjectByIdWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIndexWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIndexWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeProjectByIndexWithoutUserId")
    public JAXBElement<RemoveProjectByIndexWithoutUserId> createRemoveProjectByIndexWithoutUserId(RemoveProjectByIndexWithoutUserId value) {
        return new JAXBElement<RemoveProjectByIndexWithoutUserId>(_RemoveProjectByIndexWithoutUserId_QNAME, RemoveProjectByIndexWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIndexWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIndexWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeProjectByIndexWithoutUserIdResponse")
    public JAXBElement<RemoveProjectByIndexWithoutUserIdResponse> createRemoveProjectByIndexWithoutUserIdResponse(RemoveProjectByIndexWithoutUserIdResponse value) {
        return new JAXBElement<RemoveProjectByIndexWithoutUserIdResponse>(_RemoveProjectByIndexWithoutUserIdResponse_QNAME, RemoveProjectByIndexWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeProjectWithoutUserId")
    public JAXBElement<RemoveProjectWithoutUserId> createRemoveProjectWithoutUserId(RemoveProjectWithoutUserId value) {
        return new JAXBElement<RemoveProjectWithoutUserId>(_RemoveProjectWithoutUserId_QNAME, RemoveProjectWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeProjectWithoutUserIdResponse")
    public JAXBElement<RemoveProjectWithoutUserIdResponse> createRemoveProjectWithoutUserIdResponse(RemoveProjectWithoutUserIdResponse value) {
        return new JAXBElement<RemoveProjectWithoutUserIdResponse>(_RemoveProjectWithoutUserIdResponse_QNAME, RemoveProjectWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeTaskByIdWithoutUserId")
    public JAXBElement<RemoveTaskByIdWithoutUserId> createRemoveTaskByIdWithoutUserId(RemoveTaskByIdWithoutUserId value) {
        return new JAXBElement<RemoveTaskByIdWithoutUserId>(_RemoveTaskByIdWithoutUserId_QNAME, RemoveTaskByIdWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeTaskByIdWithoutUserIdResponse")
    public JAXBElement<RemoveTaskByIdWithoutUserIdResponse> createRemoveTaskByIdWithoutUserIdResponse(RemoveTaskByIdWithoutUserIdResponse value) {
        return new JAXBElement<RemoveTaskByIdWithoutUserIdResponse>(_RemoveTaskByIdWithoutUserIdResponse_QNAME, RemoveTaskByIdWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeTaskWithoutUserId")
    public JAXBElement<RemoveTaskWithoutUserId> createRemoveTaskWithoutUserId(RemoveTaskWithoutUserId value) {
        return new JAXBElement<RemoveTaskWithoutUserId>(_RemoveTaskWithoutUserId_QNAME, RemoveTaskWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeTaskWithoutUserIdResponse")
    public JAXBElement<RemoveTaskWithoutUserIdResponse> createRemoveTaskWithoutUserIdResponse(RemoveTaskWithoutUserIdResponse value) {
        return new JAXBElement<RemoveTaskWithoutUserIdResponse>(_RemoveTaskWithoutUserIdResponse_QNAME, RemoveTaskWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByEmail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByEmail }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserByEmail")
    public JAXBElement<RemoveUserByEmail> createRemoveUserByEmail(RemoveUserByEmail value) {
        return new JAXBElement<RemoveUserByEmail>(_RemoveUserByEmail_QNAME, RemoveUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByEmailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByEmailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserByEmailResponse")
    public JAXBElement<RemoveUserByEmailResponse> createRemoveUserByEmailResponse(RemoveUserByEmailResponse value) {
        return new JAXBElement<RemoveUserByEmailResponse>(_RemoveUserByEmailResponse_QNAME, RemoveUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserById")
    public JAXBElement<RemoveUserById> createRemoveUserById(RemoveUserById value) {
        return new JAXBElement<RemoveUserById>(_RemoveUserById_QNAME, RemoveUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserByIdResponse")
    public JAXBElement<RemoveUserByIdResponse> createRemoveUserByIdResponse(RemoveUserByIdResponse value) {
        return new JAXBElement<RemoveUserByIdResponse>(_RemoveUserByIdResponse_QNAME, RemoveUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserByIndex")
    public JAXBElement<RemoveUserByIndex> createRemoveUserByIndex(RemoveUserByIndex value) {
        return new JAXBElement<RemoveUserByIndex>(_RemoveUserByIndex_QNAME, RemoveUserByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserByIndexResponse")
    public JAXBElement<RemoveUserByIndexResponse> createRemoveUserByIndexResponse(RemoveUserByIndexResponse value) {
        return new JAXBElement<RemoveUserByIndexResponse>(_RemoveUserByIndexResponse_QNAME, RemoveUserByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserByLogin")
    public JAXBElement<RemoveUserByLogin> createRemoveUserByLogin(RemoveUserByLogin value) {
        return new JAXBElement<RemoveUserByLogin>(_RemoveUserByLogin_QNAME, RemoveUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserByLoginResponse")
    public JAXBElement<RemoveUserByLoginResponse> createRemoveUserByLoginResponse(RemoveUserByLoginResponse value) {
        return new JAXBElement<RemoveUserByLoginResponse>(_RemoveUserByLoginResponse_QNAME, RemoveUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBackup }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBackup }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveBackup")
    public JAXBElement<SaveBackup> createSaveBackup(SaveBackup value) {
        return new JAXBElement<SaveBackup>(_SaveBackup_QNAME, SaveBackup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBackupResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBackupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveBackupResponse")
    public JAXBElement<SaveBackupResponse> createSaveBackupResponse(SaveBackupResponse value) {
        return new JAXBElement<SaveBackupResponse>(_SaveBackupResponse_QNAME, SaveBackupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBase64Data }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBase64Data }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveBase64Data")
    public JAXBElement<SaveBase64Data> createSaveBase64Data(SaveBase64Data value) {
        return new JAXBElement<SaveBase64Data>(_SaveBase64Data_QNAME, SaveBase64Data.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBase64DataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBase64DataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveBase64DataResponse")
    public JAXBElement<SaveBase64DataResponse> createSaveBase64DataResponse(SaveBase64DataResponse value) {
        return new JAXBElement<SaveBase64DataResponse>(_SaveBase64DataResponse_QNAME, SaveBase64DataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinaryData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBinaryData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveBinaryData")
    public JAXBElement<SaveBinaryData> createSaveBinaryData(SaveBinaryData value) {
        return new JAXBElement<SaveBinaryData>(_SaveBinaryData_QNAME, SaveBinaryData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinaryDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBinaryDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveBinaryDataResponse")
    public JAXBElement<SaveBinaryDataResponse> createSaveBinaryDataResponse(SaveBinaryDataResponse value) {
        return new JAXBElement<SaveBinaryDataResponse>(_SaveBinaryDataResponse_QNAME, SaveBinaryDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonFasterXmlData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveJsonFasterXmlData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveJsonFasterXmlData")
    public JAXBElement<SaveJsonFasterXmlData> createSaveJsonFasterXmlData(SaveJsonFasterXmlData value) {
        return new JAXBElement<SaveJsonFasterXmlData>(_SaveJsonFasterXmlData_QNAME, SaveJsonFasterXmlData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonFasterXmlDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveJsonFasterXmlDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveJsonFasterXmlDataResponse")
    public JAXBElement<SaveJsonFasterXmlDataResponse> createSaveJsonFasterXmlDataResponse(SaveJsonFasterXmlDataResponse value) {
        return new JAXBElement<SaveJsonFasterXmlDataResponse>(_SaveJsonFasterXmlDataResponse_QNAME, SaveJsonFasterXmlDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonJaxBData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveJsonJaxBData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveJsonJaxBData")
    public JAXBElement<SaveJsonJaxBData> createSaveJsonJaxBData(SaveJsonJaxBData value) {
        return new JAXBElement<SaveJsonJaxBData>(_SaveJsonJaxBData_QNAME, SaveJsonJaxBData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonJaxBDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveJsonJaxBDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveJsonJaxBDataResponse")
    public JAXBElement<SaveJsonJaxBDataResponse> createSaveJsonJaxBDataResponse(SaveJsonJaxBDataResponse value) {
        return new JAXBElement<SaveJsonJaxBDataResponse>(_SaveJsonJaxBDataResponse_QNAME, SaveJsonJaxBDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlFasterXmlData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveXmlFasterXmlData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveXmlFasterXmlData")
    public JAXBElement<SaveXmlFasterXmlData> createSaveXmlFasterXmlData(SaveXmlFasterXmlData value) {
        return new JAXBElement<SaveXmlFasterXmlData>(_SaveXmlFasterXmlData_QNAME, SaveXmlFasterXmlData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlFasterXmlDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveXmlFasterXmlDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveXmlFasterXmlDataResponse")
    public JAXBElement<SaveXmlFasterXmlDataResponse> createSaveXmlFasterXmlDataResponse(SaveXmlFasterXmlDataResponse value) {
        return new JAXBElement<SaveXmlFasterXmlDataResponse>(_SaveXmlFasterXmlDataResponse_QNAME, SaveXmlFasterXmlDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlJaxBData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveXmlJaxBData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveXmlJaxBData")
    public JAXBElement<SaveXmlJaxBData> createSaveXmlJaxBData(SaveXmlJaxBData value) {
        return new JAXBElement<SaveXmlJaxBData>(_SaveXmlJaxBData_QNAME, SaveXmlJaxBData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlJaxBDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveXmlJaxBDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveXmlJaxBDataResponse")
    public JAXBElement<SaveXmlJaxBDataResponse> createSaveXmlJaxBDataResponse(SaveXmlJaxBDataResponse value) {
        return new JAXBElement<SaveXmlJaxBDataResponse>(_SaveXmlJaxBDataResponse_QNAME, SaveXmlJaxBDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveYamlData }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveYamlData }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveYamlData")
    public JAXBElement<SaveYamlData> createSaveYamlData(SaveYamlData value) {
        return new JAXBElement<SaveYamlData>(_SaveYamlData_QNAME, SaveYamlData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveYamlDataResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveYamlDataResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "saveYamlDataResponse")
    public JAXBElement<SaveYamlDataResponse> createSaveYamlDataResponse(SaveYamlDataResponse value) {
        return new JAXBElement<SaveYamlDataResponse>(_SaveYamlDataResponse_QNAME, SaveYamlDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowAllUserWithProject }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ShowAllUserWithProject }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "showAllUserWithProject")
    public JAXBElement<ShowAllUserWithProject> createShowAllUserWithProject(ShowAllUserWithProject value) {
        return new JAXBElement<ShowAllUserWithProject>(_ShowAllUserWithProject_QNAME, ShowAllUserWithProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowAllUserWithProjectResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ShowAllUserWithProjectResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "showAllUserWithProjectResponse")
    public JAXBElement<ShowAllUserWithProjectResponse> createShowAllUserWithProjectResponse(ShowAllUserWithProjectResponse value) {
        return new JAXBElement<ShowAllUserWithProjectResponse>(_ShowAllUserWithProjectResponse_QNAME, ShowAllUserWithProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowUserList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ShowUserList }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "showUserList")
    public JAXBElement<ShowUserList> createShowUserList(ShowUserList value) {
        return new JAXBElement<ShowUserList>(_ShowUserList_QNAME, ShowUserList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowUserListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ShowUserListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "showUserListResponse")
    public JAXBElement<ShowUserListResponse> createShowUserListResponse(ShowUserListResponse value) {
        return new JAXBElement<ShowUserListResponse>(_ShowUserListResponse_QNAME, ShowUserListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SizeProjectWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SizeProjectWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "sizeProjectWithoutUserId")
    public JAXBElement<SizeProjectWithoutUserId> createSizeProjectWithoutUserId(SizeProjectWithoutUserId value) {
        return new JAXBElement<SizeProjectWithoutUserId>(_SizeProjectWithoutUserId_QNAME, SizeProjectWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SizeProjectWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SizeProjectWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "sizeProjectWithoutUserIdResponse")
    public JAXBElement<SizeProjectWithoutUserIdResponse> createSizeProjectWithoutUserIdResponse(SizeProjectWithoutUserIdResponse value) {
        return new JAXBElement<SizeProjectWithoutUserIdResponse>(_SizeProjectWithoutUserIdResponse_QNAME, SizeProjectWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SizeTaskWithoutUserId }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SizeTaskWithoutUserId }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "sizeTaskWithoutUserId")
    public JAXBElement<SizeTaskWithoutUserId> createSizeTaskWithoutUserId(SizeTaskWithoutUserId value) {
        return new JAXBElement<SizeTaskWithoutUserId>(_SizeTaskWithoutUserId_QNAME, SizeTaskWithoutUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SizeTaskWithoutUserIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SizeTaskWithoutUserIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "sizeTaskWithoutUserIdResponse")
    public JAXBElement<SizeTaskWithoutUserIdResponse> createSizeTaskWithoutUserIdResponse(SizeTaskWithoutUserIdResponse value) {
        return new JAXBElement<SizeTaskWithoutUserIdResponse>(_SizeTaskWithoutUserIdResponse_QNAME, SizeTaskWithoutUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SizeUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SizeUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "sizeUser")
    public JAXBElement<SizeUser> createSizeUser(SizeUser value) {
        return new JAXBElement<SizeUser>(_SizeUser_QNAME, SizeUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SizeUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SizeUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "sizeUserResponse")
    public JAXBElement<SizeUserResponse> createSizeUserResponse(SizeUserResponse value) {
        return new JAXBElement<SizeUserResponse>(_SizeUserResponse_QNAME, SizeUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByEmail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserByEmail }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "unlockUserByEmail")
    public JAXBElement<UnlockUserByEmail> createUnlockUserByEmail(UnlockUserByEmail value) {
        return new JAXBElement<UnlockUserByEmail>(_UnlockUserByEmail_QNAME, UnlockUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByEmailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserByEmailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "unlockUserByEmailResponse")
    public JAXBElement<UnlockUserByEmailResponse> createUnlockUserByEmailResponse(UnlockUserByEmailResponse value) {
        return new JAXBElement<UnlockUserByEmailResponse>(_UnlockUserByEmailResponse_QNAME, UnlockUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "unlockUserByLogin")
    public JAXBElement<UnlockUserByLogin> createUnlockUserByLogin(UnlockUserByLogin value) {
        return new JAXBElement<UnlockUserByLogin>(_UnlockUserByLogin_QNAME, UnlockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.goloshchapov.ru/", name = "unlockUserByLoginResponse")
    public JAXBElement<UnlockUserByLoginResponse> createUnlockUserByLoginResponse(UnlockUserByLoginResponse value) {
        return new JAXBElement<UnlockUserByLoginResponse>(_UnlockUserByLoginResponse_QNAME, UnlockUserByLoginResponse.class, null, value);
    }

}
