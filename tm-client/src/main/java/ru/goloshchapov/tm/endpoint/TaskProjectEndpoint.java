package ru.goloshchapov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.3
 * 2021-05-01T16:33:12.862+03:00
 * Generated source version: 3.4.3
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.goloshchapov.ru/", name = "TaskProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByUserIdRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByUserIdResponse")
    @RequestWrapper(localName = "removeAllTaskByUserId", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByUserId")
    @ResponseWrapper(localName = "removeAllTaskByUserIdResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeAllTaskByUserId(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByProjectIdRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByProjectIdResponse")
    @RequestWrapper(localName = "removeAllTaskByProjectId", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByProjectId")
    @ResponseWrapper(localName = "removeAllTaskByProjectIdResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByProjectIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.goloshchapov.tm.endpoint.Task> removeAllTaskByProjectId(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/isEmptyProjectWithTaskByIndexRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/isEmptyProjectWithTaskByIndexResponse")
    @RequestWrapper(localName = "isEmptyProjectWithTaskByIndex", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.IsEmptyProjectWithTaskByIndex")
    @ResponseWrapper(localName = "isEmptyProjectWithTaskByIndexResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.IsEmptyProjectWithTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean isEmptyProjectWithTaskByIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectIndex", targetNamespace = "")
        java.lang.Integer projectIndex
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeProjectByIndexWithTaskRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeProjectByIndexWithTaskResponse")
    @RequestWrapper(localName = "removeProjectByIndexWithTask", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveProjectByIndexWithTask")
    @ResponseWrapper(localName = "removeProjectByIndexWithTaskResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveProjectByIndexWithTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.Project removeProjectByIndexWithTask(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectIndex", targetNamespace = "")
        java.lang.Integer projectIndex
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/isEmptyProjectWithTaskByIdRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/isEmptyProjectWithTaskByIdResponse")
    @RequestWrapper(localName = "isEmptyProjectWithTaskById", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.IsEmptyProjectWithTaskById")
    @ResponseWrapper(localName = "isEmptyProjectWithTaskByIdResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.IsEmptyProjectWithTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean isEmptyProjectWithTaskById(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeProjectByIdWithTaskRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeProjectByIdWithTaskResponse")
    @RequestWrapper(localName = "removeProjectByIdWithTask", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveProjectByIdWithTask")
    @ResponseWrapper(localName = "removeProjectByIdWithTaskResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveProjectByIdWithTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.Project removeProjectByIdWithTask(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/findAllTaskByProjectNameRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/findAllTaskByProjectNameResponse")
    @RequestWrapper(localName = "findAllTaskByProjectName", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.FindAllTaskByProjectName")
    @ResponseWrapper(localName = "findAllTaskByProjectNameResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.FindAllTaskByProjectNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.goloshchapov.tm.endpoint.Task> findAllTaskByProjectName(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectName", targetNamespace = "")
        java.lang.String projectName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/findAllTaskByProjectIdRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/findAllTaskByProjectIdResponse")
    @RequestWrapper(localName = "findAllTaskByProjectId", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.FindAllTaskByProjectId")
    @ResponseWrapper(localName = "findAllTaskByProjectIdResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.FindAllTaskByProjectIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.goloshchapov.tm.endpoint.Task> findAllTaskByProjectId(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/unbindTaskFromProjectByIdRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/unbindTaskFromProjectByIdResponse")
    @RequestWrapper(localName = "unbindTaskFromProjectById", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.UnbindTaskFromProjectById")
    @ResponseWrapper(localName = "unbindTaskFromProjectByIdResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.UnbindTaskFromProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.Task unbindTaskFromProjectById(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByProjectIndexRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByProjectIndexResponse")
    @RequestWrapper(localName = "removeAllTaskByProjectIndex", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByProjectIndex")
    @ResponseWrapper(localName = "removeAllTaskByProjectIndexResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByProjectIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.goloshchapov.tm.endpoint.Task> removeAllTaskByProjectIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectIndex", targetNamespace = "")
        java.lang.Integer projectIndex
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/findAllTaskByProjectIndexRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/findAllTaskByProjectIndexResponse")
    @RequestWrapper(localName = "findAllTaskByProjectIndex", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.FindAllTaskByProjectIndex")
    @ResponseWrapper(localName = "findAllTaskByProjectIndexResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.FindAllTaskByProjectIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.goloshchapov.tm.endpoint.Task> findAllTaskByProjectIndex(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectIndex", targetNamespace = "")
        java.lang.Integer projectIndex
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/isEmptyProjectWithTaskByNameRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/isEmptyProjectWithTaskByNameResponse")
    @RequestWrapper(localName = "isEmptyProjectWithTaskByName", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.IsEmptyProjectWithTaskByName")
    @ResponseWrapper(localName = "isEmptyProjectWithTaskByNameResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.IsEmptyProjectWithTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean isEmptyProjectWithTaskByName(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectName", targetNamespace = "")
        java.lang.String projectName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByProjectNameRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeAllTaskByProjectNameResponse")
    @RequestWrapper(localName = "removeAllTaskByProjectName", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByProjectName")
    @ResponseWrapper(localName = "removeAllTaskByProjectNameResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveAllTaskByProjectNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.goloshchapov.tm.endpoint.Task> removeAllTaskByProjectName(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectName", targetNamespace = "")
        java.lang.String projectName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeProjectByNameWithTaskRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/removeProjectByNameWithTaskResponse")
    @RequestWrapper(localName = "removeProjectByNameWithTask", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveProjectByNameWithTask")
    @ResponseWrapper(localName = "removeProjectByNameWithTaskResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.RemoveProjectByNameWithTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.Project removeProjectByNameWithTask(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "projectName", targetNamespace = "")
        java.lang.String projectName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/bindTaskToProjectByIdRequest", output = "http://endpoint.tm.goloshchapov.ru/TaskProjectEndpoint/bindTaskToProjectByIdResponse")
    @RequestWrapper(localName = "bindTaskToProjectById", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.BindTaskToProjectById")
    @ResponseWrapper(localName = "bindTaskToProjectByIdResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.BindTaskToProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.Task bindTaskToProjectById(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    );
}
