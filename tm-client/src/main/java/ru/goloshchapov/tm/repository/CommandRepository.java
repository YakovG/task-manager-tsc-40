package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    final Predicate<String> checkString = s -> !isEmpty(s);

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        InitCommands();
    }

    @SneakyThrows
    private void InitCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.goloshchapov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.goloshchapov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            commandList.add(clazz.newInstance());
        }
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }


    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        commandList.stream()
                .filter(command -> (checkString.test(command.name())))
                .collect(Collectors.toList())
                .forEach(c -> result.add(c.name()));
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getArgumentNames() {
        @NotNull final List<String> result = new ArrayList<>();
        commandList.stream()
                .filter(command -> (checkString.test(command.arg())))
                .collect(Collectors.toList())
                .forEach(c -> result.add(c.arg()));
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getArgCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        commandList.stream()
                .filter(command -> checkString.test(command.arg()))
                .collect(Collectors.toList())
                .forEach(c -> result.add(c.name()));
        return result;
    }

}
