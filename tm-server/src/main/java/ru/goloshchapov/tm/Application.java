package ru.goloshchapov.tm;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.bootstrap.Bootstrap;
import ru.goloshchapov.tm.util.SystemUtil;

public final class Application {

    public static void main(String[] args) {
        System.out.println(SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
