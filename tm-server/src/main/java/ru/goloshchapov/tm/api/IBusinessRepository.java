package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<M extends AbstractBusinessEntity> {

    void addAll(String userId, Collection<M> collection);

    @Nullable
    M add(String userId, M model);

    @Nullable
    M update(M model);

    @Nullable
    List<M> findAll(String userId);

    @Nullable
    List<M> findAll(String userId, Comparator<M> comparator);

    @Nullable
    List<M> findAllByUserId(String userId);

    @Nullable
    List<M> findAllStarted(String userId, Comparator<M> comparator);

    @Nullable
    List<M> findAllCompleted(String userId, Comparator<M> comparator);

    @Nullable
    M findOneById(String userId, String modelId);

    @Nullable
    M findOneByIndex(String userId, Integer index);

    @Nullable
    M findOneByName(String name);

    @Nullable
    M findOneByName(String userId, String name);

    boolean isAbsentById(@NotNull String userId, @NotNull String modelId);

    boolean isAbsentByName(String name);

    boolean isAbsentByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByName(String name);

    int size(String userId);

    void remove(String userId, M model);

    void clear(String userId);

    @Nullable
    M removeOneById(String userId, String modelId);

    @Nullable
    M removeOneByIndex(String userId, Integer index);

    @Nullable
    M removeOneByName(String userId, String name);

    @Nullable
    M startOneById(String userId, String modelId);

    @Nullable
    M startOneByIndex(String userId, Integer index);

    @Nullable
    M startOneByName(String userId, String name);

    @Nullable
    M finishOneById(String userId, String modelId);

    @Nullable
    M finishOneByIndex(String userId, Integer index);

    @Nullable
    M finishOneByName(String userId, String name);

}
