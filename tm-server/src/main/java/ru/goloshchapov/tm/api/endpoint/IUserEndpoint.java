package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {
    @WebMethod
    @SneakyThrows
    @NotNull User setUserPassword(
            @WebParam(name = "session") Session session,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @SneakyThrows
    @NotNull User updateUser(
            @WebParam(name = "session") Session session,
            @WebParam(name = "firstName") String firstName,
            @WebParam(name = "lastName") String lastName,
            @WebParam(name = "middleName") String middleName
    );

    User getUser(
            @WebParam(name = "session") Session session
    );
}
