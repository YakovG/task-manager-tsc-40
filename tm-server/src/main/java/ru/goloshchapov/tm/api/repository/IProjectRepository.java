package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO `tm_project` (`id`, `name`, `description`, `userId`, `status`, " +
            "`created`, `dateStart`, `dateFinish`) " +
            "VALUES(#{id}, #{name}, #{description}, #{userId}, #{status}, #{created}, #{dateStart}, #{dateFinish})")
    void add(@NotNull Project project);

    @Update("UPDATE `tm_project` SET `name` = #{name}, `description` = #{description}, `userId` = #{userId}, " +
            "`status` = #{status}, `created` = #{created}, `dateStart` = #{dateStart}, " +
            "`dateFinish` = #{dateFinish} WHERE `id` = #{id}")
    void update(@NotNull Project project);

    @Nullable
    @Select("SELECT * FROM `tm_project`")
    List<Project> findAllProjects();

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `userId` = #{userId}")
    List<Project> findAll(@NotNull String userId);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `userId` = #{userId}")
    List<Project> findAllByUserId(@NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `id` = #{projectId} AND `userId` = #{userId} LIMIT 1")
    Project findOneById(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("projectId") String projectId
    );

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `id` = #{projectId} LIMIT 1")
    Project findProjectById(@NotNull final @Param("projectId") String projectId);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `name` = #{name} LIMIT 1")
    Project findProjectByName(@NotNull final @Param("name") String name);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `name` = #{name} AND `userId` = #{userId} LIMIT 1")
    Project findOneByName(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("name") String name);

    @Delete("DELETE FROM `tm_project` WHERE `id` = #{projectId} AND `userId` = #{userId}")
    void removeOneById(@NotNull final String userId, @NotNull final String projectId);

    @Delete("DELETE FROM `tm_project` WHERE `id` = #{projectId}")
    void removeProjectById(@NotNull final String projectId);

    @Select("DELETE FROM `tm_project` WHERE `userId` = #{userId}")
    void clear(@NotNull final String userId);

    @Select("DELETE FROM `tm_project`")
    void clearAll();

    @Select("SELECT COUNT(*) FROM `tm_project`")
    int size();
}
