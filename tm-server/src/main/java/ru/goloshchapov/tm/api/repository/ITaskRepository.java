package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO `tm_task` (`id`, `name`, `description`, `userId`, `projectId`, `status`, " +
            "`created`, `dateStart`, `dateFinish`) " +
            "VALUES(#{id}, #{name}, #{description}, #{userId}, #{projectId}, #{status}, " +
            "#{created}, #{dateStart}, #{dateFinish})")
    void add(@NotNull Task task);

    @Update("UPDATE `tm_task` SET `name` = #{name}, `description` = #{description}, `userId` = #{userId}, " +
            "`projectId` = #{projectId}, `status` = #{status}, `created` = #{created}, " +
            "`dateStart` = #{dateStart}, `dateFinish` = #{dateFinish} WHERE `id` = #{id}")
    void update(@NotNull Task task);

    @Nullable
    @Select("SELECT * FROM `tm_task`")
    List<Task> findAllTasks();

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId}")
    List<Task> findAll(@NotNull String userId);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId}")
    List<Task> findAllByUserId(@NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `id` = #{taskId} AND `userId` = #{userId} LIMIT 1")
    Task findOneById(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("taskId") String taskId
    );

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `id` = #{taskId} LIMIT 1")
    Task findTaskById(@NotNull final @Param("taskId") String taskId);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `name` = #{name} LIMIT 1")
    Task findTaskByName(@NotNull final @Param("name") String name);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `name` = #{name} AND `userId` = #{userId} LIMIT 1")
    Task findOneByName(
            @NotNull final @Param("userId") String userId,
            @NotNull final @Param("name") String name
    );

    @Delete("DELETE FROM `tm_task` WHERE `id` = #{taskId} AND `userId` = #{userId}")
    void removeOneById(@NotNull final String userId, @NotNull final String taskId);

    @Delete("DELETE FROM `tm_task` WHERE `id` = #{taskId}")
    void removeTaskById(@NotNull final String taskId);

    @Select("DELETE FROM `tm_task` WHERE `userId` = #{userId}")
    void clear(@NotNull final String userId);

    @Select("DELETE FROM `tm_task`")
    void clearAll();

    @Select("SELECT COUNT(*) FROM `tm_task`")
    int size();

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId} AND `projectId` = #{projectId}")
    List<Task> findAllByProjectId(
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("projectId") String projectId
    );

    @Update("UPDATE `tm_task` SET `projectId` = #{projectId} " +
            "WHERE `userId` = #{userId} AND `id` = #{taskId}")
    void bindToProjectById (
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("taskId") String taskId,
            @Nullable final @Param("projectId") String projectId
    );

    @Update("UPDATE `tm_task` SET `projectId` = #{projectId} " +
            "WHERE `userId` = #{userId} AND `id` = #{taskId}")
    void unbindFromProjectById (
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("taskId") String taskId,
            @Nullable final @Param("projectId") String projectId
    );


    @Delete("DELETE FROM `tm_task` WHERE `projectId` = #{projectId} AND `userId` = #{userId}")
    void removeAllByProjectId(
            @Nullable final @Param("userId") String userId,
            @Nullable final @Param("projectId") String projectId
    );
}
