package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.Update;

public interface ITestRepository {

    @Update("CREATE TABLE `tm_user` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`login` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`passwordHash` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`firstname` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`lastname` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`middlename` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`role` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`locked` BIT(1) NULL DEFAULT NULL, " +
            "PRIMARY KEY (`id`) USING BTREE " +
            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;")
    void initTestUserTable();

    @Update("CREATE TABLE `tm_session` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`timestamp` BIGINT(20) NOT NULL DEFAULT '0', " +
            "`userId` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`signature` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "PRIMARY KEY (`id`) USING BTREE, " +
            "INDEX `FK_tm_session_tm_user` (`userId`) USING BTREE " +
            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;")
    void initTestSessionTable();

    @Update("CREATE TABLE `tm_project` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`created` TIMESTAMP NULL DEFAULT NULL, " +
            "`dateStart` TIMESTAMP NULL DEFAULT NULL, " +
            "`dateFinish` TIMESTAMP NULL DEFAULT NULL, " +
            "PRIMARY KEY (`id`) USING BTREE, " +
            "INDEX `FK_tm_project_tm_user` (`userId`) USING BTREE " +
            ") COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;")
    void initTestProjectTable();

    @Update("CREATE TABLE `tm_task` ( " +
            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
            "`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`projectId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
            "`created` TIMESTAMP NULL DEFAULT NULL, " +
            "`dateStart` TIMESTAMP NULL DEFAULT NULL, " +
            "`dateFinish` TIMESTAMP NULL DEFAULT NULL, " +
            "PRIMARY KEY (`id`) USING BTREE, " +
            "INDEX `FK_tm_task_tm_user` (`userId`) USING BTREE, " +
            "INDEX `FK_tm_task_tm_project` (`projectId`) USING BTREE " +
            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;")
    void initTestTaskTable();

    @Update("DROP DATABASE IF EXISTS testtm;")
    void dropDatabase();

}
