package ru.goloshchapov.tm.api.repository;


import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO `tm_user` " +
            "(`id`,`login`,`passwordHash`,`email`,`firstname`,`lastname`,`middlename`,`role`,`locked`) " +
            "VALUES(#{id}, #{login}, #{passwordHash}, #{email}, " +
            "#{firstname}, #{lastname}, #{middlename}, #{role}, #{locked})")
    void add(User user);

    @Nullable
    @Select("SELECT * FROM `tm_user`")
    List<User> findAllUsers();

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `id` = #{id} LIMIT 1")
    User findUserById(@NotNull String id);

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `login` = #{login} LIMIT 1")
    User findUserByLogin(@Nullable String login);

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `email` = #{email} LIMIT 1")
    User findUserByEmail(@Nullable String email);

    @Nullable
    @Select("DELETE FROM `tm_user` WHERE `id` = #{userId}")
    void removeUserById(@Nullable String userId);

    @Nullable
    @Update("UPDATE `tm_user` SET `login` = #{login}, `passwordHash` = #{passwordHash}, `email` = #{email}, " +
            "`firstname` = #{firstname}, `lastname` = #{lastname}, `middlename` = #{middlename}, " +
            "`role` = #{role}, `locked` = #{locked} WHERE `id` = #{id}")
    void update(@NotNull User user);

    @Delete("DELETE FROM `tm_user`")
    void clear();

    @Select("SELECT COUNT(*) FROM `tm_user`")
    int size();

}
