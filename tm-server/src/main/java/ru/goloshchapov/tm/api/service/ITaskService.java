package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    @Nullable Task add (String userId, String name, String description);

    @Nullable
    @SneakyThrows
    Task add(@Nullable String userId, @Nullable Task task);

    @Nullable
    @SneakyThrows
    Task update(@Nullable Task task);

    @SneakyThrows
    boolean isAbsentById(@NotNull String userId, @NotNull String taskId);

    @SneakyThrows
    boolean isAbsentByName(@NotNull String userId, @NotNull String taskName);

    @Nullable
    @SneakyThrows
    List<Task> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<Task> findAllByUserId(@Nullable String userId);

    @Nullable
    @SneakyThrows
    Task findOneById(@Nullable String userId, @Nullable String taskId);

    @Nullable
    @SneakyThrows
    Task findOneByName(@Nullable String name);

    @Nullable
    @SneakyThrows
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    @SneakyThrows
    Task removeOneById(@Nullable String userId, @Nullable String taskId);

    @SneakyThrows
    void clear(@Nullable String userId);

}
