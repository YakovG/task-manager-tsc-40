package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User getRootUser();

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    @Nullable
    @SneakyThrows
    User create(@Nullable User user);

    @Nullable
    User create(String login, String password);

    @Nullable
    User create(String login, String password, String email);

    @Nullable
    User create(String login, String password, Role role);

    @Nullable
    User create(String login, String password, String email, String role);

    @Nullable User add(@Nullable User user);

    @NotNull
    @SneakyThrows
    List<User> findAll();

    User findUserById(@Nullable String id);

    User findOneById(@Nullable String id);

    @NotNull
    User findUserByLogin(String login);

    @NotNull
    User findUserByEmail(String email);

    @Nullable
    User removeUser(User user);

    @Override
    User removeOneById(@Nullable String id);

    @Nullable
    User removeUserByLogin(String login);

    @Nullable
    User removeUserByEmail(String email);

    @Nullable
    User setPassword(String userId, String password);

    @Nullable
    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

    @Nullable
    User lockUserByLogin(String login);

    @Nullable
    User unlockUserByLogin(String login);

    @Nullable
    User lockUserByEmail(String email);

    @Nullable
    User unlockUserByEmail(String email);

    void createTestUser();
}
