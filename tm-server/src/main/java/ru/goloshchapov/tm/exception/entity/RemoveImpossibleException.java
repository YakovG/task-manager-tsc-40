package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class RemoveImpossibleException extends AbstractException {

    public RemoveImpossibleException() {
        super("Error! No objects to remove...");
    }
}
