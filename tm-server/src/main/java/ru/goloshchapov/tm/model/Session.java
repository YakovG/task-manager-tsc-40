package ru.goloshchapov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public final class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Getter
    @Setter
    private Long timestamp;

    @Getter
    @Setter
    private String userId;

    @Getter
    @Setter
    private String signature;
    
}
