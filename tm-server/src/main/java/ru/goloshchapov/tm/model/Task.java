package ru.goloshchapov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class Task extends AbstractBusinessEntity {

    @Nullable
    private String projectId = null;

}