package ru.goloshchapov.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.*;
import ru.goloshchapov.tm.api.service.IConnectionService;

import javax.sql.DataSource;

public final class ConnectionService implements IConnectionService {

    private final IPropertyService propertyService;

    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(IPropertyService propertyService) {
        this.propertyService = propertyService;
        sqlSessionFactory = getSqlSessionFactory();
    }

    @Override
    @NotNull
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String url = propertyService.getJdbcUrl();
        @NotNull final String driver = propertyService.getJdbcDriver();
        @NotNull final String user = propertyService.getJdbcUsername();
        @NotNull final String password = propertyService.getJdbcPassword();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITestRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
