package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.service.ITaskService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.model.Task;

import java.util.List;
import java.util.Optional;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull private final ServiceLocator serviceLocator;

    @NotNull
    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task task) {
        if (task == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
            return task;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            task.setUserId(userId);
            taskRepository.add(task);
            sqlSession.commit();
            return task;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @Override
    @Nullable
    @SneakyThrows
    public Task add(@Nullable final String userId, @Nullable final Task task) {
        if (task == null) throw new ElementsNotFoundException();
        if (isEmpty(userId)) throw new EmptyIdException();
        task.setUserId(userId);
        return add(task);
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task update(@Nullable Task task) {
        if (task == null) return null;
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
            return task;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw  e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final List<Task> tasks = taskRepository.findAllTasks();
            if (tasks == null) throw new ElementsNotFoundException();
            return tasks;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final List<Task> tasks = taskRepository.findAll(userId);
            if (tasks == null) throw new ElementsNotFoundException();
            return tasks;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final List<Task> tasks = taskRepository.findAllByUserId(userId);
            if (tasks == null) throw new ElementsNotFoundException();
            return tasks;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable final String taskId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneById(userId, taskId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String taskId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findTaskById(taskId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task findOneByName(@Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findTaskByName(name);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneByName(userId, name);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneById(@Nullable final String userId, @Nullable final String taskId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @Nullable final Task result = findOneById(userId, taskId);
        if (result == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeOneById(userId, taskId);
            sqlSession.commit();
            return result;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneById(@Nullable final String taskId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        @Nullable final Task result = findOneById(taskId);
        if (result == null) throw new ElementsNotFoundException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeTaskById(taskId);
            sqlSession.commit();
            return result;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear(userId);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
