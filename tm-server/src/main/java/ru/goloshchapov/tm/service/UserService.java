package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.api.service.IUserService;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.constant.RootUserConst;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.auth.EmailExistsException;
import ru.goloshchapov.tm.exception.auth.LoginExistsException;
import ru.goloshchapov.tm.exception.empty.*;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.exception.entity.UserByEmailNotFoundException;
import ru.goloshchapov.tm.exception.entity.UserByIdNotFoundException;
import ru.goloshchapov.tm.exception.entity.UserByLoginNotFoundException;
import ru.goloshchapov.tm.exception.incorrect.RoleIncorrectException;
import ru.goloshchapov.tm.model.User;

import java.util.List;

import static ru.goloshchapov.tm.util.HashUtil.salt;
import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final User rootUser;

    @NotNull private final ServiceLocator serviceLocator;

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
        rootUser = new User();
        rootUser.setLogin(RootUserConst.ROOT_LOGIN);
        @Nullable final String passwordHash = salt(serviceLocator.getPropertyService(), RootUserConst.ROOT_PASSWORD);
        rootUser.setPasswordHash(passwordHash);
        rootUser.setRole(Role.ADMIN);
    }

    @NotNull
    @Override
    public User getRootUser() {
        return rootUser;
    }

    @Override
    @SneakyThrows
    public final boolean isLoginExists(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findUserByLogin(login) != null;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean isEmailExists(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findUserByEmail(email) != null;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User create(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        @Nullable final String passwordSalt = salt(serviceLocator.getPropertyService(), password);
        if (isEmpty(passwordSalt)) throw new EmptyPasswordHashException();
        user.setPasswordHash(passwordSalt);
        return create(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password, @Nullable final String email
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyIdException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @Nullable final User user = create(login, password);
        user.setEmail(email);
        return create(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password, @Nullable final Role role
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final Role[] roles = Role.values();
        @NotNull final String roleChecking = role.toString();
        if (!checkInclude(roleChecking,toStringArray(roles))) throw new RoleIncorrectException(roleChecking);
        @Nullable final User user = create(login, password);
        user.setRole(role);
        return create(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email, @Nullable final String role
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (isEmpty(role)) throw new EmptyRoleException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull Role[] roles = Role.values();
        if (!checkInclude(role, toStringArray(roles))) throw new RoleIncorrectException(role);
        @NotNull final Role roleChecked = Role.valueOf(role);
        @Nullable final User user = create(login, password, email);
        user.setRole(roleChecked);
        return create(user);
    }

    @Override
    @Nullable
    public User add(@Nullable final User user) {
        if (user == null) return null;
        return create(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final List<User> users = userRepository.findAllUsers();
            if (users == null) throw new ElementsNotFoundException();
            return users;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findUserById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findUserById(id);
            if (user == null) throw new UserByIdNotFoundException(id);
            return user;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return findUserById(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (login.equals(RootUserConst.ROOT_LOGIN)) return rootUser;
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findUserByLogin(login);
            if (user == null) throw new UserByLoginNotFoundException(login);
            return user;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findUserByEmail(email);
            if (user == null) throw new UserByEmailNotFoundException(email);
            return user;
        } catch (final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeUserById(user.getId());
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User removeOneById(@Nullable String id) {
        if (id == null) throw new EmptyIdException();
        @Nullable final User user = findUserById(id);
        return removeUser(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (!isLoginExists(login)) throw new UserByLoginNotFoundException(login);
        @NotNull final User user = findUserByLogin(login);
        return removeUser(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        if (!isEmailExists(email)) throw new UserByEmailNotFoundException(email);
        @NotNull final User user = findUserByEmail(email);
        return removeUser(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final User user = findOneById(userId);
        if (user == null) throw new UserByIdNotFoundException(userId);
        @Nullable final String hash = salt(serviceLocator.getPropertyService(), password);
        if (isEmpty(hash)) throw new EmptyPasswordHashException();
        user.setPasswordHash(hash);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @Nullable final User user = findUserById(userId);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setMiddlename(middleName);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (!isLoginExists(login)) throw new UserByLoginNotFoundException(login);
        @NotNull final User user = findUserByLogin(login);
        user.setLocked(true);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (!isLoginExists(login)) throw new UserByLoginNotFoundException(login);
        @NotNull final User user = findUserByLogin(login);
        user.setLocked(false);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User lockUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        if (!isEmailExists(email)) throw new UserByEmailNotFoundException(email);
        @NotNull final User user = findUserByEmail(email);
        user.setLocked(true);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User unlockUserByEmail(@Nullable final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        if (!isEmailExists(email)) throw new UserByEmailNotFoundException(email);
        @NotNull final User user = findUserByEmail(email);
        user.setLocked(false);
        @NotNull final SqlSession sqlSession = serviceLocator.getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void createTestUser() {
        @NotNull User user = new User();
        user.setId("aaa");
        user.setLogin("admin");
        @Nullable String password = salt(serviceLocator.getPropertyService(), "admin");
        user.setPasswordHash(password);
        user.setRole(Role.ADMIN);
        add(user);
        user = new User();
        user.setId("ttt");
        user.setLogin("test");
        password = salt(serviceLocator.getPropertyService(),"test");
        user.setPasswordHash(password);
        user.setEmail("test@test.tt");
        add(user);
        user = new User();
        user.setId("ddd");
        user.setLogin("demo");
        password = salt(serviceLocator.getPropertyService(),"demo");
        user.setPasswordHash(password);
        user.setEmail("demo@demo.dd");
        add(user);
    }
}
