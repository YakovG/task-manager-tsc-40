package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

public final class ProjectRepositoryTest implements ServiceLocator{

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestProjectTable();
    }

    @After
    @SneakyThrows
    public void after() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        projectRepository.clearAll();
        sqlSession.commit();
        sqlSession.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testCreate() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        Assert.assertTrue(projectRepository.findAllProjects().isEmpty());
        final Project project = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        projectRepository.add(project);
        sqlSession.commit();
        Assert.assertFalse(projectRepository.findAllProjects().isEmpty());
        final Project projectById = projectRepository.findProjectById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemove() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        Assert.assertTrue(projectRepository.findAllProjects().isEmpty());
        final Project project = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        projectRepository.add(project);
        sqlSession.commit();
        Assert.assertFalse(projectRepository.findAllProjects().isEmpty());
        final Project projectById = projectRepository.findProjectById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(projectById.getId(), project.getId());
        projectRepository.removeProjectById(projectById.getId());
        sqlSession.commit();
        Assert.assertTrue(projectRepository.findAllProjects().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testClear() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        Assert.assertTrue(projectRepository.findAllProjects().isEmpty());
        final Project project = new Project();
        final Project project1 = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project1);
        projectRepository.add(project);
        sqlSession.commit();
        projectRepository.add(project1);
        sqlSession.commit();
        Assert.assertFalse(projectRepository.findAllProjects().isEmpty());
        projectRepository.clearAll();
        sqlSession.commit();
        Assert.assertTrue(projectRepository.findAllProjects().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testSize() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        Assert.assertTrue(projectRepository.findAllProjects().isEmpty());
        final Project project = new Project();
        final Project project1 = new Project();
        Assert.assertNotNull(project);
        Assert.assertNotNull(project1);
        projectRepository.add(project);
        projectRepository.add(project1);
        Assert.assertFalse(projectRepository.findAllProjects().isEmpty());
        Assert.assertEquals(2,projectRepository.size());
        sqlSession.close();
    }
}
