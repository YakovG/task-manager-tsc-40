package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

public final class UserRepositoryTest implements ServiceLocator {

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestUserTable();
    }

    @After
    @SneakyThrows
    public void after() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        userRepository.clear();
        sqlSession.commit();
        sqlSession.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testCreate() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        userRepository.add(user);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAllUsers().isEmpty());
        final User userById = userRepository.findUserById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemove() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        userRepository.add(user);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAllUsers().isEmpty());
        final User userById = userRepository.findUserById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        userRepository.removeUserById(userById.getId());
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemoveByLogin() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        userRepository.add(user);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAllUsers().isEmpty());
        final User userById = userRepository.findUserById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userById.getLogin());
        Assert.assertEquals("test",userById.getLogin());
        final User userByLogin = userRepository.findUserByLogin("test");
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals("test", userByLogin.getLogin());
        userRepository.removeUserById(userByLogin.getId());
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemoveByEmail() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        user.setEmail("test@test");
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("test@test", user.getEmail());
        userRepository.add(user);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAllUsers().isEmpty());
        final User userById = userRepository.findUserById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        Assert.assertNotNull(userById.getEmail());
        Assert.assertEquals("test@test",userById.getEmail());
        final User userByEmail = userRepository.findUserByEmail("test@test");
        Assert.assertNotNull(userByEmail);
        Assert.assertEquals("test@test", userByEmail.getEmail());
        userRepository.removeUserById(userByEmail.getId());
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testClear() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        userRepository.add(user);
        sqlSession.commit();
        final User user1 = new User();
        Assert.assertNotNull(user1);
        Assert.assertNotNull(user1.getId());
        user1.setLogin("user");
        Assert.assertNotNull(user1.getLogin());
        Assert.assertEquals("user", user1.getLogin());
        user1.setPasswordHash("user");
        Assert.assertNotNull(user1.getPasswordHash());
        userRepository.add(user1);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAllUsers().isEmpty());
        userRepository.clear();
        sqlSession.commit();
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testSize() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        userRepository.add(user);
        sqlSession.commit();
        final User user1 = new User();
        Assert.assertNotNull(user1);
        Assert.assertNotNull(user1.getId());
        user1.setLogin("user");
        Assert.assertNotNull(user1.getLogin());
        Assert.assertEquals("user", user1.getLogin());
        user1.setPasswordHash("user");
        Assert.assertNotNull(user1.getPasswordHash());
        userRepository.add(user1);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAllUsers().isEmpty());
        Assert.assertEquals(2,userRepository.size());
        sqlSession.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testFind() {
        final SqlSession sqlSession = serviceLocator.getTestService().getSqlSession();
        final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        Assert.assertTrue(userRepository.findAllUsers().isEmpty());
        final User user = new User();
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        user.setLogin("test");
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
        user.setPasswordHash("test");
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("test", user.getPasswordHash());
        user.setEmail("test@test");
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("test@test", user.getEmail());
        userRepository.add(user);
        sqlSession.commit();
        Assert.assertFalse(userRepository.findAllUsers().isEmpty());
        Assert.assertNotNull(userRepository.findUserByLogin("test"));
        Assert.assertEquals("test", userRepository.findUserByLogin("test").getLogin());
        Assert.assertNotNull(userRepository.findUserByEmail("test@test"));
        Assert.assertEquals("test@test", userRepository.findUserByEmail("test@test").getEmail());
        sqlSession.close();
    }
}
